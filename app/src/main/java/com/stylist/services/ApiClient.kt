package com.stylist.services


import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.stylist.BuildConfig
import com.stylist.StylistApp
import com.stylist.activities.LoginActivity
import com.stylist.utils.CoreConstants
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


class ApiClient {
    companion object  {
        var authToken: String =""
        fun setToken() {
              authToken = "Bearer " + SharedPrefrence.getSessionToken(StylistApp.instance?.applicationContext)
        }
    }

    private var retrofit: Retrofit? = null
    private  val REQUEST_TIMEOUT = 60
    private var okHttpClient: OkHttpClient? = null


     private fun getClient(): Retrofit? {

         if (!Utils.isNetworkAvailable(StylistApp.instance?.applicationContext) && StylistApp.instance?.applicationContext != null) {
            Utils.showToast(
                StylistApp.instance?.applicationContext,
                "Please check your internet connection",
                Toast.LENGTH_SHORT
            )

        }
        authToken = "Bearer " + SharedPrefrence.getSessionToken(StylistApp.instance?.applicationContext)

         if (okHttpClient == null)
             StylistApp.instance?.applicationContext?.let { initOkHttp(it) }

         if (retrofit == null) {
          retrofit = Retrofit.Builder()
                 .baseUrl(BuildConfig.BASE_URL)
                 .client(okHttpClient)
                 //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                 .addConverterFactory(GsonConverterFactory.create())
                 .build()
         }
         return retrofit

    }
    private fun getClientWithoutAuth(): Retrofit {
        Log.d("ApiClient ","getClientWithoutAuth ")
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).build()
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            // .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }
    val apiService: ApiInterface = getClient()!!.create(ApiInterface::class.java)
    val apiServiceWithAuth: ApiInterface = getClientWithoutAuth().create(ApiInterface::class.java)

    private fun initOkHttp(context: Context) {
        val httpClient = OkHttpClient().newBuilder()
            .connectTimeout(REQUEST_TIMEOUT.toLong(),TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT.toLong(),TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT.toLong(),TimeUnit.SECONDS)
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(interceptor)
        httpClient.addInterceptor(object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): Response {
                val original = chain.request()
                val requestBuilder = authToken?.let {
                    original.newBuilder()
                        .addHeader("Authorization", it)
                        .addHeader("Accept", "application/json")
                        .addHeader("Request-Type", "Android")
                        .addHeader("Content-Type", "application/json")
                }
                val request = requestBuilder!!.build()
                val response = chain.proceed(request)
                Log.d("request===",request.toString())
                if (response.code == 401) {
                    //new Utils().showToast(context, context.getString(R.string.login_again), Toast.LENGTH_SHORT);
                    val intent = Intent(context,LoginActivity::class.java)
                    intent.putExtra(CoreConstants.Intent.INTENT_AUTH_ERROR, true)
                    SharedPrefrence.setLogin(context,false)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    StylistApp.instance?.startActivity(intent)
                }
                return response
            }
        })
       okHttpClient = httpClient.build()
    }
}