package com.stylist.services

import com.stylist.data.*
import com.techniexe.testing.data.NotificationCount
import com.techniexe.testing.data.NotificationResModel
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


interface ApiInterface {

    @GET("stylist/auth/mobiles/{mobileNumber}")
    suspend fun getOtpByMobNum(
        @Path("mobileNumber") mobileNumber: String?,
        @Query("method") method: String?
    ): Response<ResponseBody>

    @POST("stylist/auth/mobiles/{mobileNumber}/code/{code}")
    suspend fun verifyOtp(
        @Path("mobileNumber") mobileNumber: String?,
        @Path("code") code: String?
    ): Response<Data<LoginResponse>>

    @POST("common/sessions")
    suspend fun getSessionToken(@Body sessionRequest: SessionRequest): Response<Data<SessionResponse>>


    @GET("stylist/user/portfolio")
    suspend fun getPortFolioRes(@Query("before")before:String?,@Query("after")after:String?):Response<PortfolioResponse>

    @GET("stylist/dashboard")
    suspend fun getDashBoardData(): Response<Data<DashBoard>>

    @GET("stylist/user/feedback")
    suspend fun getFeedback( @Query("before") beforeTxt: String?): Response<FeedbackData>

    @GET("stylist/user")
    suspend fun getProfile(): Response<Data<Stylist>>


    @GET("stylist/leave")
    suspend fun getLeaveData(@Query("month") month:String?,@Query("year") year:String?,@Query("before") before:String?,@Query("after")after:String?):Response<LeaveListData>


    @GET("stylist/holiday")
    suspend fun getHolidayList(@Query("month") month:String?,@Query("year") year:String?,@Query("before") before:String?,@Query("after")after:String?,@Query("holiday_name")holiday_name:String?):Response<HolidayResponse>


    @HTTP(method = "DELETE", path = "stylist/user/media", hasBody = true)
    suspend fun deletePortfolioMedia(@Body portFolioMediaObj:PortFolioMediaObj):Response<ResponseBody>


    @Multipart
    @PATCH("stylist/user/media")
    suspend fun uploadPortFolioToServer(@Part surveyImage:ArrayList<MultipartBody.Part>):Response<ResponseBody>

    @POST("stylist/leave")
    suspend fun addLeave(@Body addLeave: AddLeave):  Response<LeaveData>

    @PATCH("stylist/user")
    suspend fun patchUserData(@Body edtUserData:EditUserPostObj):Response<ResponseBody>


    @Multipart
    @PATCH("stylist/user/media")
    suspend fun uploadUserPic(@Part userImg:MultipartBody.Part):Response<ResponseBody>


    @PATCH("stylist/leave/{leaveId}")
    suspend fun editLeave(@Body addLeave: AddLeave, @Path("leaveId") leaveId : String?):  Response<EditLeaveResponse>


    @HTTP(method = "DELETE", path = "stylist/leave/{leaveId}", hasBody = true)
    //@DELETE("stylist/leave/{leaveId}")
    suspend fun deleteLeave(@Body code: LeaveDelete, @Path("leaveId") leaveId : String?):  Response<ResponseBody>

    @GET("stylist/user/requestOTP")
    suspend fun leaveCancel():  Response<Code>


    @GET("stylist/appointment")
    suspend fun getAppoinment(@Query("start_date") startDate:String?,@Query("end_date") endDate:String?): Response<AppointmentDetailsList>

    @GET("stylist/notification")
    suspend fun getNotifications(@Query("before") before: String?, @Query("after") after: String?): Response<Data<NotificationResModel>>

    @PATCH("stylist/notification/{notificationId}")
    suspend fun notificationItemSeen(@Path("notificationId") notificationId: String?): Response<ResponseBody>

    @GET("stylist/notification/count")
    suspend fun getNotificationCount(): Response<Data<NotificationCount>>
}