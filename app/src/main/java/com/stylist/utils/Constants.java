package com.stylist.utils;

/**
 * Created by Hitesh Patel on 15,February,2021
 */
public class Constants {

    public final static int REQUEST_CODE_SELECT_IMAGE = 201;
    public final static String ACTION_START_FOREGROUND_SERVICE = "start_uplod_service";
    public final static String ACTION_STOP_FOREGROUND_SERVICE = "stop_uplod_service";
}
