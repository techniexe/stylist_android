package com.stylist.utils

import android.content.Context
import android.content.SharedPreferences
import com.stylist.R


object SharedPrefrence {

    private val DAY_OF_MONTH = "dayofmonth"
    //VARIABLES
    const val SESSION_TOKEN = "session_token"
    const val IS_LOGED_IN = "is_logged_in"
    const val USER_NAME = "user_name"
    const val USER_EMAIL = "user_email"
    const val USER_PROFILE = "user_profile"
    const val USER_DESGNATION = "user_designation"
    const val USER_BRANCH = "user_branch"

    const val USER_INFO= "user_info"
    const val USER_BIRTHDAY= "user_birthday"
    const val USER_GENDER= "user_gender"
    const val USER_MOBILE= "user_mobile"
    /*USEEMAIL*/
    const val USER_BRANCH_CONTACT= "user_branch_contact"
    const val USER_BRANCH_INFO= "user_branch_info"
    const val SALOON_TYPE= "saloon_type"
    const val BRANCH_EMAIL= "branch_email"
    const val BRANCH_LAT = "branch_lat"
    const val BRANCH_LONG = "branch_long"
    private fun getSharedPref(context: Context?): SharedPreferences? {
        return context?.getSharedPreferences(
            context?.getString(R.string.app_name),
            Context.MODE_PRIVATE
        )
    }


    fun setSessionToken(context: Context, title: String?) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SESSION_TOKEN, title)
        editor.apply()
    }

    fun getSessionToken(context: Context?): String? {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SESSION_TOKEN, "")
    }

    fun setLogin(context : Context,flag : Boolean){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putBoolean(IS_LOGED_IN, flag)
        editor.apply()
    }

    fun getLogin(context :Context?): Boolean {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getBoolean(IS_LOGED_IN, false)
    }

    fun setUserName(context: Context?,name:String?){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_NAME, name)
        editor.apply()
    }
    fun getUserName(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_NAME, "")
    }
    fun setUserEmail(context: Context?,email:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_EMAIL, email)
        editor.apply()
    }
    fun getuserEmail(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_EMAIL, "")
    }
    fun setUserProfile(context: Context?,userProfile:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_PROFILE, userProfile)
        editor.apply()
    }
    fun getUserProfile(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_PROFILE, "")
    }
    fun setDesignation(context: Context?,designation:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_DESGNATION, designation)
        editor.apply()
    }
    fun getDesignation(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_DESGNATION, "")
    }
    fun setBranchName(context: Context?,branch:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_BRANCH, branch)
        editor.apply()
    }
    fun getBrnachName(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_BRANCH, "")
    }

    /*User_profile_AboutUs*/
    fun setUserInfo(context: Context?,userInfo:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_INFO, userInfo)
        editor.apply()
    }
    fun getUserInfo(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_INFO, "")
    }

    fun setBirthday(context: Context?,birthday:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_BIRTHDAY, birthday)
        editor.apply()
    }
    fun geBirthday(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_BIRTHDAY, "")
    }

    fun setGender(context: Context?,gender:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_GENDER, gender)
        editor.apply()
    }
    fun getGender(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_GENDER, "")
    }
    fun setMobileNo(context: Context?,mobile:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_MOBILE, mobile)
        editor.apply()
    }
    fun getMobileNo(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_MOBILE, "")
    }
    fun setBranchContact(context: Context?,mobile:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_BRANCH_CONTACT, mobile)
        editor.apply()
    }
    fun getBranchContact(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_BRANCH_CONTACT, "")
    }
    fun setBranchInfo(context: Context?,info:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(USER_BRANCH_INFO, info)
        editor.apply()
    }
    fun getBranchInfo(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(USER_BRANCH_INFO, "")
    }

    fun setDayOfMonth(context: Context, day: Int) {
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putInt(DAY_OF_MONTH, day)
        editor.apply()
    }

    fun getDayOfMonth(context: Context): Int {
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getInt(DAY_OF_MONTH, 0)
    }

    fun setSalooonType(context: Context?,type:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(SALOON_TYPE, type)
        editor.apply()
    }
    fun getSalooonType(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(SALOON_TYPE, "")
    }
    fun setBranchEmail(context: Context?,email:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(BRANCH_EMAIL, email)
        editor.apply()
    }
    fun getBranchEmail(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(BRANCH_EMAIL, "")
    }

    fun setBranchLat(context: Context?,lat:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(BRANCH_LAT, lat)
        editor.apply()
    }
    fun getBranchLat(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(BRANCH_LAT, "")
    }

    fun setBranchLong(context: Context?,lat:String){
        val sharedPref = getSharedPref(context)
        val editor = sharedPref!!.edit()
        editor.putString(BRANCH_LONG, lat)
        editor.apply()
    }
    fun getBranchLong(context: Context?):String?{
        val sharedPref = getSharedPref(context)
        return sharedPref!!.getString(BRANCH_LONG, "")
    }
}
