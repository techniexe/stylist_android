package com.stylist.utils
import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.stylist.R
import com.stylist.data.ErrorRes
import okhttp3.ResponseBody
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


object Utils {

    fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    fun showToast(context: Context?, mesaage: String, time: Int) {
        if (context != null) {
            val inflater: LayoutInflater = (context as Activity).layoutInflater
            val layout: View = inflater.inflate(
                R.layout.custom_toast,
                (context as Activity).findViewById(R.id.toast_layout_root) as ViewGroup?
            )


            val text = layout.findViewById<View>(R.id.text) as TextView

            text.text = mesaage

            val toast = Toast(context)

            //toast.setGravity(Gravity.BOTTOM, 0, 0)
            toast.duration = Toast.LENGTH_LONG
            toast.view = layout
            toast.show()
        }

    }
    fun setPrecesionFormate(number: Double?): String {
        val form = DecimalFormat("##,##,##,##,###.##")
        return form.format(number)
    }
    fun setErrorData(context: Context, data: ResponseBody?) {
        val res = data!!.string().toString()
        val gson = Gson()
        val type = object : TypeToken<ErrorRes>() {}.type
        val err = gson.fromJson<ErrorRes>(res, type)
        if (err?.error?.message != null && err.error.message.isNotEmpty())
            showToast(context, err.error.message, Toast.LENGTH_SHORT)


    }
    fun hideKeyboard(view: View) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    fun covertTimeToText(dataDate: String?): String? {
        var convTime: String? = null
        val prefix = ""
        val suffix = "Ago"
        try {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val pasTime = dateFormat.parse(dataDate)
            val nowTime = Date()
            val dateDiff = nowTime.time - pasTime.time
            val second: Long = TimeUnit.MILLISECONDS.toSeconds(dateDiff)
            val minute: Long = TimeUnit.MILLISECONDS.toMinutes(dateDiff)
            val hour: Long = TimeUnit.MILLISECONDS.toHours(dateDiff)
            val day: Long = TimeUnit.MILLISECONDS.toDays(dateDiff)
            if (second < 60) {
                convTime = "$second Seconds $suffix"
            } else if (minute < 60) {
                convTime = "$minute Minutes $suffix"
            } else if (hour < 24) {
                convTime = "$hour Hours $suffix"
            } else if (day >= 7) {
                convTime = if (day > 360) {
                    (day / 360).toString() + " Years " + suffix
                } else if (day > 30) {
                    (day / 30).toString() + " Months " + suffix
                } else {
                    (day / 7).toString() + " Week " + suffix
                }
            } else if (day < 7) {
                convTime = "$day Days $suffix"
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            e.message?.let { Log.e("ConvTimeE", it) }
        }
        return convTime
    }



    fun setImageUsingGlide(context: Context, imageUrl: String?, imageView: ImageView) {
        val requestOptions = RequestOptions().apply {
            placeholder(com.stylist.R.color.colorGrey)
            error(com.stylist.R.color.borderColor)
            diskCacheStrategy(DiskCacheStrategy.ALL)
        }


        Glide.with(context)
            .setDefaultRequestOptions(requestOptions)
            .load(imageUrl ?: "")
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable,
                    model: Any,
                    target: com.bumptech.glide.request.target.Target<Drawable>,
                    dataSource: DataSource,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }
            })
            .into(imageView)


    }
    fun getConvertDate(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)

            val birthdateFormat = SimpleDateFormat("dd MMM, yyyy ")

            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }
    fun getConvertBirthDate(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)

            val birthdateFormat = SimpleDateFormat("dd MMM, yyyy")

            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }
    fun getTimes(time: String): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)

           return date.time
        } catch (e: ParseException) {

        }
        return 0L
    }
    fun getTime(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)

            val birthdateFormat = SimpleDateFormat("hh:mm a")

            val formatedDate = birthdateFormat.format(date)

            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }

    fun GlidewithLoadUriImageNew(context: Context?, url: Uri?, imageView: ImageView) {
        if (context != null) {
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_user_placeholder)
            requestOptions.error(R.drawable.ic_user_placeholder)
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
            requestOptions.skipMemoryCache(true)
            Glide.with(context)
                .setDefaultRequestOptions(requestOptions)
                .load(url ?: "")
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {

                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: com.bumptech.glide.request.target.Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {


                        return false
                    }
                })
                .into(imageView)
        }
    }

    fun GlidewithLoadUrlImageNew(context: Context?, url: String?, imageView: ImageView) {
        if (context != null) {
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_user_placeholder)
            requestOptions.error(R.drawable.ic_user_placeholder)
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE)
            Glide.with(context)
                    .setDefaultRequestOptions(requestOptions)
                    .load(url ?: "")
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: com.bumptech.glide.request.target.Target<Drawable>,
                            isFirstResource: Boolean
                        ): Boolean {

                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable,
                            model: Any,
                            target: com.bumptech.glide.request.target.Target<Drawable>,
                            dataSource: DataSource,
                            isFirstResource: Boolean
                        ): Boolean {


                            return false
                        }
                    })
                    .into(imageView)
        }
    }

    fun getDate(time: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)
            // val longMiliSeconds = date.time
            val birthdateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            birthdateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val formatedDate = birthdateFormat.format(date)
            //val timeString: String = DateUtils.getRelativeTimeSpanString(longMiliSeconds, System.currentTimeMillis(), 0).toString()
            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }


    fun getSelectedMonthCode(mnt: Int):String?{

        when (mnt) {
            0 -> return "jan"
            1 -> return "feb"
            2 -> return "mar"
            3 -> return "apr"
            4 -> return "may"
            5 -> return "jun"
            6 -> return "jul"
            7 -> return "aug"
            8 -> return "sep"
            9 -> return "oct"
            10 -> return "nov"
            11 -> return "dec"


            else -> return null
        }

    }
    fun getDateTime(year: Int, month: Int, dayOfMonth: Int): Long {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)

        return calendar.time.time
    }
    fun setTimeToMidnight(date: Date):Date{
        val calendar = Calendar.getInstance()
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    fun getIsoDate(time: Long): String {

        val dateFormat = SimpleDateFormat("dd-MM-yyyy")
        //dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = Date(time)
            // val longMiliSeconds = date.time
            val birthdateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            //birthdateFormat.timeZone = TimeZone.getTimeZone("GMT")
            val formatedDate = birthdateFormat.format(date)
            //val timeString: String = DateUtils.getRelativeTimeSpanString(longMiliSeconds, System.currentTimeMillis(), 0).toString()
            return formatedDate.toString()
        } catch (e: ParseException) {

        }
        return ""
    }
    fun setSpanColor(
        context: Context,
        view: TextView,
        fulltext: String,
        subtext: String,
        color: Int
    ) {
        view.setText(fulltext, TextView.BufferType.SPANNABLE)
        val str: Spannable = view.text as Spannable
        val i = fulltext.indexOf(subtext)
        str.setSpan(
            ForegroundColorSpan(ContextCompat.getColor(context, color)),
            i,
            i + subtext.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    inline fun Context.readAttributes(
        attrs: AttributeSet? = null,
        stylebleResource: IntArray,
        attributeReaderFun: (TypedArray) -> Unit = {}
    ) {
        val typedArray = obtainStyledAttributes(attrs, stylebleResource)
        attributeReaderFun(typedArray)
        typedArray.recycle()
    }
    fun getConvertDateToLong(time: String): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        dateFormat.timeZone = TimeZone.getTimeZone("GMT")
        try {
            val date = dateFormat.parse(time)


            return date.time
        } catch (e: ParseException) {

        }
        return 0
    }
    fun isSameday(`when`: Date?, now: Date?): Boolean {
        val time = Calendar.getInstance()
        time.time = `when`
        val thenYear = time[Calendar.YEAR]
        val thenMonth = time[Calendar.MONTH]
        val thenMonthDay = time[Calendar.DAY_OF_MONTH]
        time.time = now
        return (thenYear == time[Calendar.YEAR]
                && thenMonth == time[Calendar.MONTH]
                && thenMonthDay == time[Calendar.DAY_OF_MONTH])
    }

    fun AppointmentStatusUpdateUi(status:String?,statustxt:TextView,context: Context){
        if (status.equals(context.getString(R.string.unapproved_txt_status))) {

            statustxt.text = context.getString(R.string.unapproved_txt_status)
            statustxt.setTextColor(Color.parseColor("#FF8C00"))

        }
        else if (status.equals(context.getString(R.string.cancelled_txt_status))) {

            statustxt.text = "CANCELLED"
            statustxt.setTextColor(Color.parseColor("#FF2828"))

        } else if (status.equals(context.getString(R.string.missed_txt_status))) {
            statustxt.text = context.getString(R.string.expired_txt_status)
            statustxt.setTextColor(Color.parseColor("#FF2828"))
        }else if (status.equals(context.getString(R.string.approved_txt_status))) {
            statustxt.text = context.getString(R.string.approved_txt_status)
            statustxt.setTextColor(Color.parseColor("#05C270"))
        }else if (status.equals(context.getString(R.string.completed_txt_status))) {

            statustxt.text = context.getString(R.string.completed_txt_status)
            statustxt.setTextColor(Color.parseColor("#05C270"))
        } else if (status.equals(context.getString(R.string.waitingpayment_txt_status))) {
            statustxt.text = context.getString(R.string.waitingpayment_txt_status_value)
            statustxt.setTextColor(Color.parseColor("#FF2828"))
        } else if (status.equals(context.getString(R.string.ongoing_txt_status))) {


            statustxt.text = context.getString(R.string.ongoing_txt_status)
            statustxt.setTextColor(Color.parseColor("#FF8C00"))

        }else if (status.equals(context.getString(R.string.noshow_txt_status_value))) {

            statustxt.text = context.getString(R.string.noshow_txt_status_txt)
            statustxt.setTextColor(Color.parseColor("#FF2828"))

        } else{
            statustxt.visibility = View.GONE

        }
    }






}