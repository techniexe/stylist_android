package com.stylist.utils

object CoreConstants {

    //CONSTANT
    const val IS_RELEASE = true

    class Intent {
        companion object {
            const val INTENT_AUTH_ERROR = "authError"
            const val INTENT_MOB_NUMBER = "mobNum"
            const val INTENT_ABOUT_US_URL = "aboutusurl"
            const val INTENT_ABOUT_US_TITLE = "aboutustitle"
        }
    }



    class ProfileTab{
        companion object {
            const val ABOUT = "About"
            const val PROTFOLIO = "Protfolio"
            const val REVIEWS = "Reviews"
            const val APPLYLEAVES = "Leaves"
            const val HOLIDAYS = "Holidays"

        }
    }
    class FRAGMENTS {
        companion object {
            const val DASHBOARD = "DASHBOARD"
            const val APPOINTMENT = "APPOINTMENT"
            const val PROFILE = "PROFILE"


        }
    }
    class STATUS {
        companion object {
            const val CANCELLED = "CANCELLED"
            const val REJECTED = "REJECTED"
            const val APPROVED = "APPROVED"
            const val UNAPPROVED = "UNAPPROVED"

        }
    }
    class NOTIFICATIONTYPE {
        companion object {
            //Order placed
            const val appointmentReschedule = 1
            const val appointmentRescheduleType = "AppointmentReschedule"
            const val appointmentSchedule = 2
            const val appointmentScheduleType = "AppointmentSchedule"
            const val cancelAppointmentSchedule = 3
            const val cancelAppointmentScheduleType = "CancelAppointmentSchedule"
            const val appointmentComplete = 4
            const val appointmentCompleteType = "AppointmentComplete"
            const val appointmentOngoing = 5
            const val appointmentOngoingType = "AppointmentOngoing"
            const val addReview = 6
            const val addReviewType = "AddReview"


        }
    }
}
