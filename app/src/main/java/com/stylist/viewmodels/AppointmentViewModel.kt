package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.repositary.AppointmentRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers

class AppointmentViewModel(private val appointmentRepositary: AppointmentRepositary):  ViewModel() {

    fun getAppointment(startDate: String?, EndDate: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = appointmentRepositary.getAppointment(startDate,EndDate)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AppointmentViewModel::class.java)) {
                return AppointmentViewModel(AppointmentRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}