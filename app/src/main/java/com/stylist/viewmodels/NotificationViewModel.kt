package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.repositary.NotificationRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 31,August,2021
 */
class NotificationViewModel (private val notificationRepositary: NotificationRepositary) : ViewModel() {

    fun notificationList(before: String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationList(before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun notificationCount() = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationCount()))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    fun notificationItemClick(notificationId: String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = notificationRepositary.notificationSeen(notificationId)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {
                return NotificationViewModel(NotificationRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}