package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.data.PortFolioMediaObj
import com.stylist.repositary.PortFolioRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 28,January,2021
 */
class PortFolioViewModel(private val portfolioRepositary: PortFolioRepositary) : ViewModel() {

    fun getPortfolio(before:String?,after:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = portfolioRepositary.getPortfolioData(before,after)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PortFolioViewModel::class.java)) {
                return PortFolioViewModel(PortFolioRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}