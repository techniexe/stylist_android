package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.data.SessionRequest
import com.stylist.repositary.SessionRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers

class SessionViewModel(private val sessionRepositary: SessionRepositary) : ViewModel() {

    fun getSession(sessionRequest: SessionRequest) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = sessionRepositary.getSessionToken(sessionRequest)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SessionViewModel::class.java)) {
                return SessionViewModel(SessionRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}