package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.data.EditUserPostObj
import com.stylist.repositary.AddPortFolioRepository
import com.stylist.repositary.EditUserDetailRepository
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 17,February,2021
 */
class EditUserDetailViewModel(private val editUserDetailRepository: EditUserDetailRepository) : ViewModel() {

    fun uploadUserData(editUserPostObj: EditUserPostObj) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = editUserDetailRepository.uploadUserDetail(editUserPostObj)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    fun uploadUserImage(parts:MultipartBody.Part) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = editUserDetailRepository.uploadUserImageServer(parts)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(EditUserDetailViewModel::class.java)) {
                return EditUserDetailViewModel(EditUserDetailRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}