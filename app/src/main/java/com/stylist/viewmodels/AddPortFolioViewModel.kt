package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.repositary.AddPortFolioRepository
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 16,February,2021
 */
class AddPortFolioViewModel(private val addPortfolioRepositary: AddPortFolioRepository) : ViewModel() {

    fun uploadPortFolioImages(parts:ArrayList<MultipartBody.Part>) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = addPortfolioRepositary.uploadPortfolioServer(parts)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }


    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(AddPortFolioViewModel::class.java)) {
                return AddPortFolioViewModel(AddPortFolioRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}