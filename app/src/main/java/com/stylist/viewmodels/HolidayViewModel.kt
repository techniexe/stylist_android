package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.repositary.HolidayRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 08,February,2021
 */
class HolidayViewModel(private val holidayRepositary: HolidayRepositary):  ViewModel() {

    fun getHoliday(month:String?,  year:String?,  before:String?, after:String?, holiday_name:String?) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = holidayRepositary.getHolidayList(month,year,before,after,holiday_name)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(HolidayViewModel::class.java)) {
                return HolidayViewModel(HolidayRepositary(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}