package com.stylist.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.stylist.data.PortFolioMediaObj
import com.stylist.repositary.PortFolioImageRepository
import com.stylist.repositary.PortFolioRepositary
import com.stylist.services.ApiInterface
import com.stylist.services.Resource
import kotlinx.coroutines.Dispatchers


/**
 * Created by Hitesh Patel on 12,February,2021
 */
class PortFolioImageViewModel(private val portfolioRepositary: PortFolioImageRepository) : ViewModel() {


    fun deletePortFolioMedia(idsObj: PortFolioMediaObj) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = portfolioRepositary.deletePortFolioImageData(idsObj)))
        }catch (exception:Exception){
            emit(Resource.error(data = null,message = exception.message?:"Error Occurred"))
        }
    }
    class ViewModelFactory(private val apiInterface: ApiInterface) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(PortFolioImageViewModel::class.java)) {
                return PortFolioImageViewModel(PortFolioImageRepository(apiInterface)) as T
            }
            throw IllegalArgumentException("Unknown class name")
        }

    }
}