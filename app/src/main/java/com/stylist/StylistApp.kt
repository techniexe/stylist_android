package com.stylist

import android.app.Application

class StylistApp : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this

    }

    companion object {
        var instance: StylistApp? = null
            private set
    }
}
