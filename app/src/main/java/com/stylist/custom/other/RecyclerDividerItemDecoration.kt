package com.stylist.custom.other

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stylist.utils.Utils.readAttributes


class RecyclerDividerItemDecoration @JvmOverloads constructor(val context:Context, val resId:Int=0,val marginDrawable:Int = 0)
    :RecyclerView.ItemDecoration(){

    private val ATTRS = intArrayOf(android.R.attr.listDivider)

    private var divider: Drawable? = null
//    private val marginDrawable = 0

    val TAG = "RecyclerDividerItemDecoration"


    init {
        if(resId == 0){
            context.readAttributes(stylebleResource = ATTRS){
                divider = it.getDrawable(0)
            }
        }else{
            divider = ContextCompat.getDrawable(context,resId)
        }

    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        val left = parent.paddingLeft + parent.resources.getDimensionPixelSize(marginDrawable)
        val right = parent.width - parent.paddingRight

        val childCount = parent.childCount
        for (i in 0 until childCount) {
            if (i != childCount - 1) {
                val child = parent.getChildAt(i)

                val params = child.layoutParams as RecyclerView.LayoutParams

                val top = child.bottom + params.bottomMargin
                val bottom = top + (divider?.intrinsicHeight ?:0)

                divider?.setBounds(left, top, right, bottom)
                divider?.draw(c)
            }
        }
    }
}