package com.stylist.custom

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.stylist.R


/**
 * Created by Hitesh Patel on 16,February,2021
 */
class UploadAnimation : AppCompatImageView {
    var frameAnimation: AnimationDrawable? = null

    constructor(context: Context) : super(context) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initView(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initView(context)
    }

    private fun initView(context: Context) {
        setImageResource(R.drawable.uploading_animation)
        frameAnimation = drawable as AnimationDrawable
    }

    fun start() {
        frameAnimation!!.start()
    }

    fun stop() {
        frameAnimation!!.stop()
    }
}