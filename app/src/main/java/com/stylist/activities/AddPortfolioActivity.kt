package com.stylist.activities


import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.adapter.AddPortfolioHorizontalAdapter
import com.stylist.custom.RealPathUtil
import com.stylist.data.Image
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.AddPortFolioViewModel
import gun0912.tedimagepicker.builder.TedImagePicker
import gun0912.tedimagepicker.builder.type.MediaType
import kotlinx.android.synthetic.main.add_port_folio_activity.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import spencerstudios.com.bungeelib.Bungee
import java.io.File


/**
 * Created by Hitesh Patel on 15,February,2021
 */
class AddPortfolioActivity : BaseActivity(),
    AddPortfolioHorizontalAdapter.AskAndSharePhotoAdapterListener {

    val selectedImages = ArrayList<Image>()
    var adapter: AddPortfolioHorizontalAdapter? = null
    private lateinit var addPortFolioViewModel: AddPortFolioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_port_folio_activity)

        setupViewModel()

        adapter = AddPortfolioHorizontalAdapter(this, this, selectedImages)
        val gridLayoutManager = GridLayoutManager(
            this@AddPortfolioActivity,
            3,
            RecyclerView.VERTICAL,
            false
        )
        val layoutParams = gridLayoutManager.spanSizeLookup
        addToTrellRecylerView?.layoutManager = gridLayoutManager
        addToTrellRecylerView?.adapter = adapter


        imgClose?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        submitBtn?.setOnClickListener {

            if (selectedImages.size > 0) {
                uploadImagesfun(selectedImages)
            } else {
                Utils.showToast(this, "Media can not be blank", Toast.LENGTH_SHORT)
            }


        }

    }

    private fun setupViewModel() {
        addPortFolioViewModel =
            ViewModelProvider(
                this,
                AddPortFolioViewModel.ViewModelFactory(ApiClient().apiService)
            ).get(AddPortFolioViewModel::class.java)


    }

    override fun onCameraClicked() {
        if (selectedImages.size < 5) {
            val limit = 5 - selectedImages.size


            TedImagePicker.with(this)
                .mediaType(MediaType.IMAGE)
                .zoomIndicator(false)
                .max(5, "Media can not be add more than 5")
                //.scrollIndicatorDateFormat("YYYYMMDD")
                //.buttonGravity(ButtonGravity.BOTTOM)
                //.buttonBackground(R.drawable.btn_sample_done_button)
                //.buttonTextColor(R.color.sample_yellow)
                .errorListener { message -> Log.d("ted", "message: $message") }
                .startMultiImage { list: List<Uri> -> showMultiImage(list) }


        } else {
            //toast message
            Utils.showToast(this, "Media can not be add more than 5", Toast.LENGTH_SHORT)
        }

    }

    fun showMultiImage(urilst: List<Uri>) {

        urilst.forEach {
            val slectImgeObj = Image(
                System.nanoTime(),
                System.currentTimeMillis().toString(),
                it.toString()
            )
            selectedImages.add(slectImgeObj)
        }
        adapter?.notifyDataSetChanged()
    }


    override fun onDeleteClick(position: Int) {
        try {
            selectedImages.removeAt(position)
            adapter?.notifyDataSetChanged()
        } catch (e: Exception) {

        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        imgClose?.performClick()
    }

    fun uploadImagesfun(selectedImg: ArrayList<Image>) {

        uploadImage.visibility = View.VISIBLE
        uploadImage.start()
        val parts: ArrayList<MultipartBody.Part> = ArrayList()



        for (index in 0 until selectedImg.size) {

            val realpath =
                RealPathUtil.getRealPath(this, Uri.parse(selectedImg.get(index).getPath()))
            val file = File(realpath)
            val surveyBody = RequestBody.create("image/jpeg".toMediaTypeOrNull(), file)
            val royalbody =
                MultipartBody.Part.createFormData("portfolio[" + index + "]", file.name, surveyBody)
            parts.add(royalbody)

        }


        if (parts.size > 0) {


            ApiClient.setToken()

            addPortFolioViewModel.uploadPortFolioImages(parts)
                .observe(this) {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCESS -> {
                                uploadImage.visibility = View.GONE
                                uploadImage.stop()
                                if (resource.data?.isSuccessful!!) {
                                    finish()
                                    Bungee.slideRight(this)
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }

                            }
                            Status.ERROR -> {
                                uploadImage.visibility = View.GONE
                                uploadImage.stop()
                                Utils.showToast(
                                    this,
                                    it.message.toString(),
                                    Toast.LENGTH_LONG
                                )
                            }
                            Status.LOADING -> {
                                uploadImage.visibility = View.VISIBLE
                                uploadImage.start()

                            }

                        }
                    }
                }
        }
    }
}