package com.stylist.activities


import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.stylist.BuildConfig
import com.stylist.R
import com.stylist.data.SessionRequest
import com.stylist.data.SessionResponse
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.CoreConstants
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import com.stylist.viewmodels.LoginViewModel
import com.stylist.viewmodels.SessionViewModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.avLoading
import kotlinx.android.synthetic.main.activity_otp.*
import spencerstudios.com.bungeelib.Bungee
import java.util.*


class LoginActivity : BaseActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var sessionViewModel: SessionViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        stopAnim()

        setupViewModel()


        etMobileNo.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signIn(v)
            }
            false
        }
        btnSignIn.setOnClickListener { v ->

            if(validateFields()){
                signIn(v)
            }

        }

    }
    private fun getSessionToken(mobNum: String) {
        ApiClient.setToken()
        val sessionRequest = SessionRequest(BuildConfig.AUTH_TOKEN, UUID.randomUUID().toString())

        sessionViewModel.getSession(sessionRequest = sessionRequest)
            .observe(this) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    setData(
                                        response.body()?.data,
                                        mobNum
                                    )
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            }
    }
    private fun setData(response: SessionResponse?, userName: String?) {
        if (response != null) {
            SharedPrefrence.setSessionToken(this, response.token)
        }

        if (userName != null) {
            logIn(userName)
        }
    }
    private fun signIn(view: View) {
        Utils.hideKeyboard(view)
        if (Utils.isNetworkAvailable(this)) {
            var userName=etMobileNo.text.toString()
            val phoneUtil = PhoneNumberUtil.getInstance()

            if (validateFields()) {
                try {

                    val numberProto = phoneUtil.parse(userName, ccp.selectedCountryNameCode)
                    val mobNum =
                        phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

                    if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                            numberProto
                        ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
                    ) {
                        getSessionToken(mobNum)
                    } else {

                        Utils.showToast(
                            this,
                            getString(R.string.err_invalid_number),
                            Toast.LENGTH_SHORT
                        )
                    }
                } catch (e: NumberParseException) {




                }
            }
        } else {
            Utils.showToast(this, getString(R.string.no_internet_msg), Toast.LENGTH_SHORT)
        }
    }

    private fun validateFields(): Boolean {
        if (etMobileNo.text.toString().isNullOrEmpty()) {
            Utils.showToast(
                this,
                getString(R.string.err_ragister_mobile_number),
                Toast.LENGTH_SHORT
            )
            return false
        }

        return true
    }
    private fun startAnim() {
        if (avLoading != null)
            avLoading.show()

    }

    private fun stopAnim() {
        if (avLoading != null)
            avLoading.hide()

    }
    private fun setupViewModel() {
        sessionViewModel =
            ViewModelProvider(
                this,
                SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
            ).get(SessionViewModel::class.java)

        loginViewModel =
            ViewModelProvider(this, LoginViewModel.ViewModelFactory(ApiClient().apiService))
                .get(LoginViewModel::class.java)

    }

    private fun logIn(mobNum :String) {
        ApiClient.setToken()
        //reverseTimer(30)
        loginViewModel.getOtpbyMob(mobNum, "sms").observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                            Utils.showToast(
                                this,
                                getString(R.string.err_send),
                                Toast.LENGTH_SHORT
                            )
                            navigateToMainActivity(mobNum)
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })
    }
    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
    }


    private fun navigateToMainActivity(mobNum: String) {
            val intent = Intent(this@LoginActivity, OTPActivity::class.java)
             intent.putExtra(CoreConstants.Intent.INTENT_MOB_NUMBER,mobNum)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)

    }

}

