package com.stylist.activities

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.aghajari.zoomhelper.ZoomHelper
import com.stylist.R
import com.stylist.data.PortFolioMediaObj
import com.stylist.data.PortFolioObj
import com.stylist.dialog.BreakAlertDialog
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.PortFolioImageViewModel
import kotlinx.android.synthetic.main.image_detail_screen.*
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 12,February,2021
 */
class ImageDetailScreenActivity:BaseActivity(), BreakAlertDialog.ClickListener{
    var url:String? = null
    val zoomHelper = ZoomHelper()
    var positionImg:Int?= null
    var portFolioObj: PortFolioObj?= null
    private lateinit var portFolioImageViewModel: PortFolioImageViewModel
    val idArray:ArrayList<String> = ArrayList()
    var deleteBtnBoolean:Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.image_detail_screen)

        url = intent.getStringExtra("imageUrl")
        positionImg = intent.getIntExtra("position",-1)?:null
        portFolioObj = intent.getParcelableExtra("obj")?:null
        deleteBtnBoolean = intent.getBooleanExtra("deleted",false)?:null
        setupViewModel()


        if(deleteBtnBoolean?:false){
            deletebtn?.visibility = View.GONE
        }

        ZoomHelper.getInstance().minScale = 1f
        ZoomHelper.getInstance().maxScale = Float.MAX_VALUE
        ZoomHelper.getInstance().shadowColor = Color.BLACK
        ZoomHelper.getInstance().maxShadowAlpha = 0.6f
        ZoomHelper.getInstance().shadowAlphaFactory = 4f
        ZoomHelper.getInstance().dismissDuration = 200
        ZoomHelper.getInstance().layoutTheme = android.R.style.Theme_Translucent_NoTitleBar_Fullscreen
        ZoomHelper.getInstance().isEnabled = true

        imgFull.layoutParams.height = resources.displayMetrics.widthPixels * 350 / 300
        ZoomHelper.Companion.addZoomableView(imgFull)

        Utils.setImageUsingGlide(this,url,imgFull)


        deletebtn?.setOnClickListener {
            val fragment: BreakAlertDialog = BreakAlertDialog(this, getString(R.string.txt_delete_image_portfolio), getString(R.string.yes_btn_txt), getString(R.string.no_btn_txt))
            fragment.show(supportFragmentManager, "alert")
            fragment.isCancelable = false
            fragment.setDialogListener(this)
        }


        navBackBtn?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }



    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        return zoomHelper.dispatchTouchEvent(ev!!,this) || super.dispatchTouchEvent(ev)
    }

    private fun setupViewModel() {
        portFolioImageViewModel =
            ViewModelProvider(this, PortFolioImageViewModel.ViewModelFactory(ApiClient().apiService))
                .get(PortFolioImageViewModel::class.java)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        navBackBtn?.performClick()

    }

    override fun onDoneClicked() {

        runOnUiThread {
            idArray.add(portFolioObj?._id.toString())
            val obj = PortFolioMediaObj(idArray)
            deletePortFolioImages(obj,positionImg)
        }


    }

    override fun onCancelClicked() {

    }


    private fun deletePortFolioImages(ids: PortFolioMediaObj, pos:Int?){
        ApiClient.setToken()
        portFolioImageViewModel.deletePortFolioMedia(ids)
            .observe(this) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    val intent = Intent()
                                    intent.putExtra("position",positionImg)
                                    intent.putExtra("objPort",portFolioObj)
                                    setResult(Activity.RESULT_OK, intent)
                                    finish()
                                    Bungee.slideRight(this)
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(
                                this,
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            }
    }

}