package com.stylist.activities

import android.graphics.Bitmap
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat
import com.stylist.R
import com.stylist.utils.CoreConstants
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import spencerstudios.com.bungeelib.Bungee

/**
 * Created by Mala Ruparel on 19/8/20.
 */
class PrivacyPolicyActivity : BaseActivity(), View.OnClickListener {

    var url: String? = null
    var title: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        changeStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        loaderView.visibility = View.VISIBLE
        getIntentData()
        tvCategoryTitle.text = title

        aboutusWebView.settings.loadsImagesAutomatically = true
        aboutusWebView.settings.javaScriptEnabled = true
        aboutusWebView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        aboutusWebView.loadUrl(url?:"")
        aboutusWebView.webViewClient = object : WebViewClient() {


            override fun onPageFinished(view: WebView, url: String) {
                loaderView.visibility = View.GONE
                // view.loadUrl("javascript:document.getElementsByTagName('body')[0].id = 'spotapp'; void(0);")
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {


            }

        }

        imgBack.setOnClickListener(this)


    }



    private fun getIntentData() {

        url = intent.getStringExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL)
        title = intent.getStringExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        Bungee.slideRight(this)
    }




    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.imgBack -> {
                finish()
                Bungee.slideRight(this)
            }
        }
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        // Check if the key event was the Back button and if there's history
        if (keyCode == KeyEvent.KEYCODE_BACK && aboutusWebView.canGoBack()) {
            aboutusWebView.goBack()
            return true
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event)
    }
}