package com.stylist.activities


import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.adapter.NotificationListAdapter
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.NotificationViewModel
import com.techniexe.testing.data.notificationObj
import kotlinx.android.synthetic.main.commno_order_toolbar.*
import kotlinx.android.synthetic.main.fragment_notifications.*
import spencerstudios.com.bungeelib.Bungee


class NotificationsActivity : BaseActivity(),NotificationListAdapter.NotificationItemClick {

    var notificationListAdapter: NotificationListAdapter? = null
    var beforeTime: String? = null
    var isLoading = true
    var notificationList = ArrayList<notificationObj>()
    private lateinit var notificationViewModel: NotificationViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_notifications)
        orderIdTitle.text = "Notification"
        setUpViewModel()

        navBackMyOrderList.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }

        rvNotification?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0) {
                    val visibleItemCount = rvNotification?.layoutManager?.childCount
                    val totalItemCount = rvNotification?.layoutManager?.itemCount
                    val pastVisiblesItems =
                        (rvNotification?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (isLoading) {

                        if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                            isLoading = false
                            if (notificationListAdapter != null) {

                                notificationListAdapter!!.loading()
                                beforeTime = notificationList[notificationList.size - 1].created_at

                                val handler = Handler()
                                handler.postDelayed(Runnable {

                                    getNotificationList(beforeTime)

                                }, 500)


                            }
                        }
                    }
                }

            }
        })
        startAnim()
        getNotificationList(beforeTime)


    }


    private fun getNotificationList(beforeTime: String?) {
        ApiClient.setToken()
        notificationViewModel.notificationList(beforeTime,  null)
            .observe(this) {
                it.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful == true) {
                                resource.data?.let { it1 -> setUpReviewUI(it1.body()?.data?.notifications) }
                                emptyView()
                            } else {
                                emptyView()
                                Utils.setErrorData(this, resource.data?.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            stopAnim()
                            emptyView()
                            Utils.showToast(
                                this,
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {

                            startAnim()
                        }

                    }
                }
            }
    }
    private fun setUpViewModel() {
        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)
    }

    private fun setUpReviewUI(rvArrayList: ArrayList<notificationObj>?) {
        if (!rvArrayList.isNullOrEmpty()) {


            if (notificationListAdapter == null) {

                notificationList.addAll(rvArrayList)

                notificationListAdapter = NotificationListAdapter(this, notificationList)


                notificationListAdapter?.notificationItemClick(this)
                rvNotification?.adapter = notificationListAdapter
            } else {

                notificationList.addAll(rvArrayList)
                notificationListAdapter?.addLoadMoreLikes(notificationList)
                isLoading = true
                notificationListAdapter?.loadDone()
            }
        }else{
            isLoading = true
            notificationListAdapter?.loadDone()
        }
    }



    override fun onBackPressed() {
        super.onBackPressed()
        navBackMyOrderList?.performClick()
    }

    private fun startAnim() {
        if (avLoading != null)
            avLoading.show()

    }

    private fun stopAnim() {
        if (avLoading != null)
            avLoading.hide()

    }

    private fun emptyView() {
        stopAnim()
        if (notificationList.isNullOrEmpty()) {
            rvNotification?.visibility = View.GONE
            llNodataNoti?.visibility = View.VISIBLE
        } else {
            llNodataNoti?.visibility = View.GONE
            rvNotification?.visibility = View.VISIBLE
        }
    }

    override fun itemClick(notificationdta: notificationObj) {
        if (notificationdta != null) {
            ApiClient.setToken()
            notificationViewModel.notificationItemClick(notificationdta?._id).observe(this, {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            stopAnim()
                            if (resource.data?.isSuccessful!!) {
                                //_id?.let { it1 -> navigateToMainList(it1) }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }
                        }
                        Status.ERROR -> {
                            stopAnim()
                            Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                        }
                        Status.LOADING -> {
                            startAnim()
                        }
                    }
                }

            })
        }
    }
}



