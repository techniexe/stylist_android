package com.stylist.activities

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.stylist.R
import com.stylist.custom.drawer.AdvanceDrawerLayout
import com.stylist.data.SessionRequest
import com.stylist.data.SessionResponse
import com.stylist.data.StylistDetails
import com.stylist.dialog.BreakAlertDialog
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.CoreConstants
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import com.stylist.viewmodels.NotificationViewModel
import com.stylist.viewmodels.SessionViewModel
import com.stylist.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.app_bar_default.*
import kotlinx.android.synthetic.main.includer_bottom_navigation.*
import kotlinx.android.synthetic.main.includer_top_navigation.*
import kotlinx.android.synthetic.main.nav_header_main.*
import spencerstudios.com.bungeelib.Bungee
import java.util.*


class MainActivity : BaseActivity() , NavigationView.OnNavigationItemSelectedListener,
    View.OnClickListener {

    //https://stackoverflow.com/questions/64537042/how-to-make-toolbar-and-statusbar-color-gradient-from-top-to-bottom
    private var drawer: AdvanceDrawerLayout? = null
    private lateinit var sessionViewModel: SessionViewModel
    private lateinit var userViewModel: UserViewModel
    private lateinit var notificationViewModel: NotificationViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ContextCompat.getColor(this, R.color.colorPrimary)?.let { changeStatusBarColor(it) }
        setupViewModel()
        getSessionToken()

        tvLogout.setOnClickListener(this)
        tvPrivacyPolicy.setOnClickListener(this)
        tvTerms.setOnClickListener(this)
        tcFaq.setOnClickListener(this)
        fmNotificationbar.setOnClickListener(this)
    }
    private fun setupViewModel() {

        sessionViewModel =
                ViewModelProvider(
                        this,
                        SessionViewModel.ViewModelFactory(ApiClient().apiServiceWithAuth)
                ).get(SessionViewModel::class.java)
        userViewModel =
                ViewModelProvider(this, UserViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(UserViewModel::class.java)
        notificationViewModel =
            ViewModelProvider(
                this,
                NotificationViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(NotificationViewModel::class.java)
    }
    private fun setBottomNavigation() {

        //navigation Architecture to open fragment
        val navView: BottomNavigationView = findViewById(R.id.nav_view_bottom)

        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(
                setOf(
                        R.id.navigation_dashboard,
                        R.id.navigation_appointment,
                        R.id.navigation_profile
                )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            if(destination.id == R.id.navigation_dashboard) {
                fmNotificationbar.visibility = View.VISIBLE
                profile_more_menu.visibility=View.GONE
                tvMainTitle.visibility=View.VISIBLE
                tvMainTitle.setText(R.string.title_dashboard)
                rlSubTIttle.visibility=View.GONE
            } else  if(destination.id == R.id.navigation_appointment)  {
                profile_more_menu.visibility=View.GONE
                fmNotificationbar.visibility = View.GONE
                tvMainTitle.visibility=View.GONE
                rlSubTIttle.visibility=View.VISIBLE
                tvTitle.setText(R.string.title_appointment)
            }else  if(destination.id == R.id.navigation_profile)  {
                profile_more_menu.visibility=View.VISIBLE
                fmNotificationbar.visibility = View.GONE
                tvMainTitle.visibility=View.VISIBLE
                rlSubTIttle.visibility=View.GONE
                tvMainTitle.setText(R.string.title_profile)

            }
        }
    }

    private fun setLeftDrawer() {

        //navigation Architecture to open left drawer
        val toolbar = setToolBar()

        drawer = findViewById<View>(R.id.drawer_layout) as AdvanceDrawerLayout
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer!!.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View>(R.id.navview) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
        drawer!!.useCustomBehavior(Gravity.START)
        drawer!!.useCustomBehavior(Gravity.END)
        toolbar.setNavigationIcon(R.drawable.ic_drawer)
        //navigation Architecture to open left drawer


        var name=SharedPrefrence.getUserName(this)
        if(!name.isNullOrEmpty())
            tvUsername.text=name

        var email=SharedPrefrence.getuserEmail(this)
        if(!email.isNullOrEmpty())
            tvUserEmail.text=email

        var profile=SharedPrefrence.getUserProfile(this)
        if(!profile.isNullOrEmpty())
            Utils.setImageUsingGlide(this, profile, imgUserProfile)
    }

    private fun setToolBar(): Toolbar {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        supportActionBar?.setDisplayShowTitleEnabled(false);
        supportActionBar?.setDisplayShowCustomEnabled(true);
        supportActionBar?.setIcon(R.drawable.ic_drawer)
        setSupportActionBar(toolbar)
        return toolbar
    }

    override fun onBackPressed() {
        if (drawer!=null && drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawer(GravityCompat.START)
        } else {
            Bungee.slideRight(this)
            finish()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (drawer!=null)
            drawer!!.closeDrawer(GravityCompat.START)

        return true
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.tvLogout -> {

                val alertDialogNew = BreakAlertDialog(object :
                    BreakAlertDialog.ClickListener {
                    override fun onDoneClicked() {
                        SharedPrefrence.setLogin(this@MainActivity, false)
                        SharedPrefrence.setUserName(this@MainActivity, "")
                        SharedPrefrence.setUserEmail(this@MainActivity, "")
                        SharedPrefrence.setUserProfile(this@MainActivity, "")
                        SharedPrefrence.setDesignation(this@MainActivity, "")
                        SharedPrefrence.setBranchName(this@MainActivity, "")
                        SharedPrefrence.setUserInfo(this@MainActivity, "")
                        SharedPrefrence.setBirthday(this@MainActivity, "")
                        SharedPrefrence.setGender(this@MainActivity, "")
                        SharedPrefrence.setMobileNo(this@MainActivity, "")
                        SharedPrefrence.setBranchContact(this@MainActivity, "")
                        SharedPrefrence.setBranchInfo(this@MainActivity, "")
                        SharedPrefrence.setSalooonType(this@MainActivity, "")
                        SharedPrefrence.setBranchEmail(this@MainActivity, "")
                        SharedPrefrence.setBranchLat(this@MainActivity,"")
                        SharedPrefrence.setBranchLong(this@MainActivity,"")
                        val intent = Intent(this@MainActivity, LoginActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        intent.flags =
                            Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                        finish()
                        Bungee.slideRight(this@MainActivity)


                    }

                    override fun onCancelClicked() {

                    }

                }, "Are you sure you want to logout?", "Logout", "Cancel")
                alertDialogNew.show(supportFragmentManager, "")



            }
            R.id.tvPrivacyPolicy -> {
                redirectActivity("http://www.google.com", getString(R.string.txt_privacy_policy))
            }
            R.id.tvTerms -> {
                redirectActivity("http://www.google.com", getString(R.string.txt_term))
            }
            R.id.tcFaq -> {
                redirectActivity("http://www.google.com", getString(R.string.txt_help))
            }
            R.id.fmNotificationbar -> {
                var intent = Intent(this, NotificationsActivity::class.java)
                startActivity(intent)
                Bungee.slideLeft(this)
            }

        }
    }
    private fun redirectActivity(url: String, title: String) {
        val intent = Intent(this, PrivacyPolicyActivity::class.java)
        intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_URL, url)
        intent.putExtra(CoreConstants.Intent.INTENT_ABOUT_US_TITLE, title)
        startActivity(intent)
        Bungee.slideLeft(this)
    }


    private fun getProfileData(){
        ApiClient.setToken()
        userViewModel.getProfile().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            storeUserDetails(resource.data.body()?.data?.stylist)
                            getNotifcationCount()
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }
            }
        })
    }
    private fun storeUserDetails(stylist: StylistDetails?) {
        if(stylist!=null) {
            if(!stylist.full_name.isNullOrEmpty())
                SharedPrefrence.setUserName(this, stylist.full_name)

            if(!stylist.email.isNullOrEmpty())
                SharedPrefrence.setUserEmail(this, stylist.email)

            if(!stylist.profile_pic.isNullOrEmpty())
                SharedPrefrence.setUserProfile(this, stylist.profile_pic)

            if(!stylist.designation.isNullOrEmpty())
                SharedPrefrence.setDesignation(this, stylist.designation)

            if(stylist.branch!=null && !stylist.branch.name.isNullOrEmpty())
                SharedPrefrence.setBranchName(this, stylist.branch.name)


            /*aboutUs*/
            if(stylist.info!=null && !stylist.info.isNullOrEmpty())
                SharedPrefrence.setUserInfo(this, stylist.info)

            if(stylist.birthdate!=null && !stylist.birthdate.isNullOrEmpty())
                SharedPrefrence.setBirthday(this, stylist.birthdate)

            if(stylist.gender!=null && !stylist.gender.isNullOrEmpty())
                SharedPrefrence.setGender(this, stylist.gender)

            if(stylist.mobile_number!=null && !stylist.mobile_number.isNullOrEmpty())
                SharedPrefrence.setMobileNo(this, stylist.mobile_number)

            if(stylist.email!=null && !stylist.email.isNullOrEmpty())
                SharedPrefrence.setUserEmail(this, stylist.email)

            if(stylist.branch!=null && !stylist.branch.contact_number.isNullOrEmpty())
                SharedPrefrence.setBranchContact(this, stylist.branch.contact_number)

            if(stylist.branch!=null && !stylist.branch.about.isNullOrEmpty())
                SharedPrefrence.setBranchInfo(this, stylist.branch.about)

            if(stylist.branch!=null && !stylist.branch.serve_gender.isNullOrEmpty())
                SharedPrefrence.setSalooonType(this, stylist.branch.serve_gender)

            if(stylist.branch!=null && !stylist.branch.location?.coordinates?.get(1).toString().isNullOrEmpty())
                SharedPrefrence.setBranchLat(this, stylist.branch.location?.coordinates?.get(1).toString())

            if(stylist.branch!=null && !stylist.branch.location?.coordinates?.get(0).toString().isNullOrEmpty())
                SharedPrefrence.setBranchLong(this, stylist.branch.location?.coordinates?.get(0).toString())

            if(stylist.vendor!=null && !stylist.vendor.email.isNullOrEmpty())
                SharedPrefrence.setBranchEmail(this, stylist.vendor.email)

        }
        setLeftDrawer()
        setBottomNavigation()
    }
    private fun getSessionToken() {
        var customToken = SharedPrefrence.getSessionToken(this)
        if (customToken.isNullOrBlank()) {
            val sessionRequest = SessionRequest(
                    "ZWFjaHBhbmVsYWZ0ZSBhYWRkZA",
                    UUID.randomUUID().toString()
            )
            sessionViewModel.getSession(sessionRequest = sessionRequest)
                    .observe(this) {
                        it?.let { resource ->
                            when (resource.status) {
                                Status.SUCESS -> {
                                    if (resource.data?.isSuccessful!!) {
                                        resource.data?.let { response ->
                                            setData(response.body()?.data)
                                        }
                                    } else {
                                        Utils.setErrorData(this, resource.data.errorBody())
                                    }

                                }
                                Status.ERROR -> {
                                    Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                                }
                                Status.LOADING -> {

                                }

                            }
                        }
                    }
        }else{
            getProfileData()
        }

    }
    private fun setData(response: SessionResponse?) {
        if (response != null) {
            SharedPrefrence.setSessionToken(this, response.token)
        }
        getProfileData()
    }

    private fun getNotifcationCount() {
        ApiClient.setToken()
        notificationViewModel.notificationCount().observe(this,Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                        //stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { response ->
                                val count = resource.data.body()?.data?.unseen_message_count ?: 0
                                runOnUiThread {
                                    if (count > 0) {
                                        notifications_badge.visibility = View.VISIBLE
                                        notifications_badge.text = count.toString()
                                    } else {
                                        notifications_badge.visibility = View.GONE
                                    }
                                }
                            }
                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }
                }
            }
        })
    }
}