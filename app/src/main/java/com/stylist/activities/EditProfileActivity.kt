package com.stylist.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.ContentResolver
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider

import androidx.lifecycle.observe
import com.google.i18n.phonenumbers.NumberParseException
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.stylist.R
import com.stylist.adapter.SelectGenderAdapter
import com.stylist.custom.RealPathUtil
import com.stylist.data.EditUserPostObj
import com.stylist.data.StylistDetails
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import com.stylist.viewmodels.EditUserDetailViewModel
import com.stylist.viewmodels.UserViewModel
import com.yalantis.ucrop.UCrop
import gun0912.tedimagepicker.builder.TedImagePicker
import gun0912.tedimagepicker.builder.type.MediaType
import kotlinx.android.synthetic.main.edit_profile_activity.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import spencerstudios.com.bungeelib.Bungee
import java.io.File
import java.util.*


/**
 * Created by Hitesh Patel on 17,February,2021
 */
class EditProfileActivity:BaseActivity() {

    var fullName:String? = null
    var aboutAs:String? = null
    var emailAdd: String? = null
    var birthDateString: String? = null
    var mobileNumber:String? = null
    var selectedGender:String? = null
    var genderArray:ArrayList<String> = ArrayList<String>()
    var genders = arrayOf("Male", "Female")
    lateinit var editUserDetailViewModel: EditUserDetailViewModel
    private lateinit var userViewModel: UserViewModel
    private var userImage: String? = null

    private var lockAspectRatio = true
    private  var setBitmapMaxWidthHeight:Boolean = false
    private var ASPECT_RATIO_X = 1
    private  var ASPECT_RATIO_Y:Int = 1
    private  var bitmapMaxWidth:Int = 1000
    private  var bitmapMaxHeight:Int = 1000
    private var IMAGE_COMPRESSION = 80
    var fileName: String? = null
    private var SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_profile_activity)


        setupViewModel()

        navBackBtn?.setOnClickListener {
            finish()
            Bungee.slideRight(this)
        }
        genderArray.addAll(genders)
        setGenderData(genderArray, genderSpinner)

        getProfileData(true, false)



        aboutMeEdt.setOnTouchListener(View.OnTouchListener { view, event ->
            if (view.id == R.id.aboutMeEdt) {
                view.parent.requestDisallowInterceptTouchEvent(true)
                when (event.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_UP -> view.parent.requestDisallowInterceptTouchEvent(false)
                }
            }
            false
        })


        tvBirthdate.setOnClickListener {
            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(
                this,
                object : DatePickerDialog.OnDateSetListener {
                    override fun onDateSet(
                        view: DatePicker?,
                        year: Int,
                        month: Int,
                        dayOfMonth: Int
                    ) {

                        birthDateString =
                            year.toString() + "-" + getNumString(month + 1) + "-" + getNumString(
                                dayOfMonth
                            ) + "T00:00:00.000Z"
                        tvBirthdate.setText(Utils.getConvertBirthDate(birthDateString!!))
                        tvBirthdate.setError(null)
                    }

                },
                newDate.get(Calendar.YEAR),
                newDate.get(Calendar.MONTH),
                newDate.get(Calendar.DAY_OF_MONTH)
            )
            val c = Calendar.getInstance();
            val mYear = c.get(Calendar.YEAR);
            val mMonth = c.get(Calendar.MONTH);
            val mDay = c.get(Calendar.DAY_OF_MONTH);
            c.set(mYear, mMonth, mDay)
            datePicker.datePicker.maxDate = c.timeInMillis
            datePicker.show()
        }


        edit_profile_pic?.setOnClickListener {
            TedImagePicker.with(this)
                .mediaType(MediaType.IMAGE)
                .zoomIndicator(false)
                .max(1, "Media can not be add more than 1")
                //.scrollIndicatorDateFormat("YYYYMMDD")
                //.buttonGravity(ButtonGravity.BOTTOM)
                //.buttonBackground(R.drawable.btn_sample_done_button)
                //.buttonTextColor(R.color.sample_yellow)
                .errorListener { message -> Log.d("ted", "message: $message") }
                .start { uri -> showSingleImage(uri) }
        }

        saveBtn?.setOnClickListener {
            if(validateFields()){



                val editUserPostObj = EditUserPostObj(
                    fullName,
                    aboutAs,
                    mobileNumber,
                    emailAdd,
                    selectedGender,
                    birthDateString,
                    null,
                    null,
                    null
                )

                uploadUserData(editUserPostObj)
            }
        }
    }


    fun showSingleImage(urig: Uri) {


        //uploadImagesfun(urig)

        cropImage(urig)

    }
    private fun setupViewModel() {
        editUserDetailViewModel =
            ViewModelProvider(
                this,
                EditUserDetailViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(EditUserDetailViewModel::class.java)
        userViewModel =
                ViewModelProvider(this, UserViewModel.ViewModelFactory(ApiClient().apiService))
                        .get(UserViewModel::class.java)
    }


    private fun cropImage(sourceUri: Uri) {


        var destinationFileName = SAMPLE_CROPPED_IMAGE_NAME
            destinationFileName += ".jpg";

        val destinationUri = Uri.fromFile( File(cacheDir, destinationFileName))

       // val destinationUri = Uri.fromFile(File(cacheDir, queryName(contentResolver, sourceUri)))
        val options = UCrop.Options()
        options.setCompressionQuality(IMAGE_COMPRESSION)
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary))
        //options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary))
        if (lockAspectRatio) options.withAspectRatio(
            ASPECT_RATIO_X.toFloat(),
            ASPECT_RATIO_Y.toFloat()
        )
        if (setBitmapMaxWidthHeight) options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeight)
        UCrop.of(sourceUri, destinationUri)
                .withOptions(options)
                .start(this)
    }

    private fun queryName(resolver: ContentResolver, uri: Uri): String? {
        val returnCursor: Cursor = resolver.query(uri, null, null, null, null)!!
        val nameIndex: Int = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name: String = returnCursor.getString(nameIndex)
        returnCursor.close()
        return name
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UCrop.REQUEST_CROP) {
            if (resultCode == Activity.RESULT_OK) {
                handleUCropResult(data)
            }
        }
    }

    fun handleUCropResult(data: Intent?){
        if (data == null) {
            setResultCancelled()
            return
        }
        val resultUri = UCrop.getOutput(data)
        Utils.GlidewithLoadUriImageNew(this, resultUri, userImageProfile)
        uploadImagesfun(resultUri!!)
    }

    private fun setResultCancelled() {
        val intent = Intent()
        setResult(RESULT_CANCELED, intent)
        finish()
    }


    private fun getProfileData(fromUpdate: Boolean, UploadImage: Boolean){
        ApiClient.setToken()
        userViewModel.getProfile().observe(this){
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {

                        if (resource.data?.isSuccessful!!) {

                            if (fromUpdate) {
                                storeUserDetails(resource.data.body()?.data?.stylist)
                            } else {
                                storeUserDetailsPrefrences(
                                    resource.data.body()?.data?.stylist,
                                    UploadImage
                                )
                            }


                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {

                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {

                    }

                }
            }
        }
    }


    private fun storeUserDetailsPrefrences(stylist: StylistDetails?, uploadImg: Boolean) {
        if(stylist!=null) {
            if(!stylist.full_name.isNullOrEmpty())
                SharedPrefrence.setUserName(this, stylist.full_name)

            if(!stylist.email.isNullOrEmpty())
                SharedPrefrence.setUserEmail(this, stylist.email)

            if(!stylist.profile_pic.isNullOrEmpty())
                SharedPrefrence.setUserProfile(this, stylist.profile_pic)

            if(!stylist.designation.isNullOrEmpty())
                SharedPrefrence.setDesignation(this, stylist.designation)

            if(stylist.branch!=null && !stylist.branch.name.isNullOrEmpty())
                SharedPrefrence.setBranchName(this, stylist.branch.name)


            /*aboutUs*/
            if(stylist.info!=null && !stylist.info.isNullOrEmpty())
                SharedPrefrence.setUserInfo(this, stylist.info)

            if(stylist.birthdate!=null && !stylist.birthdate.isNullOrEmpty())
                SharedPrefrence.setBirthday(this, stylist.birthdate)

            if(stylist.gender!=null && !stylist.gender.isNullOrEmpty())
                SharedPrefrence.setGender(this, stylist.gender)

            if(stylist.mobile_number!=null && !stylist.mobile_number.isNullOrEmpty())
                SharedPrefrence.setMobileNo(this, stylist.mobile_number)

            if(stylist.email!=null && !stylist.email.isNullOrEmpty())
                SharedPrefrence.setUserEmail(this, stylist.email)

            if(stylist.branch!=null && !stylist.branch.contact_number.isNullOrEmpty())
                SharedPrefrence.setBranchContact(this, stylist.branch.contact_number)

            if(stylist.branch!=null && !stylist.branch.about.isNullOrEmpty())
                SharedPrefrence.setBranchInfo(this, stylist.branch.about)

        }


        if(!uploadImg){
            finish()
            Bungee.slideRight(this)
        }

    }


    private fun storeUserDetails(stylist: StylistDetails?) {
        if(stylist!=null) {
            if(!stylist.full_name.isNullOrEmpty())
                edtName.setText(stylist.full_name)


            if(!stylist.email.isNullOrEmpty())
                emailEdt.setText(stylist.email)


            if(!stylist.profile_pic.isNullOrEmpty())

            Utils.GlidewithLoadUrlImageNew(this, stylist.profile_pic, userImageProfile)


            /*aboutUs*/
            if(stylist.info!=null && !stylist.info.isNullOrEmpty())
                aboutMeEdt.setText(stylist.info)

            if(stylist.birthdate!=null && !stylist.birthdate.isNullOrEmpty())
               birthDateString = stylist.birthdate
               tvBirthdate.setText(Utils.getConvertBirthDate(stylist.birthdate!!))

            if(stylist.gender!=null && !stylist.gender.isNullOrEmpty())
                if(stylist.gender == "male"){
                    selectedGender = "male"
                    genderSpinner.setSelection(0)

                }else if(stylist.gender == "female"){
                    selectedGender = "female"
                    genderSpinner.setSelection(1)
                }


            if(stylist.mobile_number!=null && !stylist.mobile_number.isNullOrEmpty())
                mobileNumberEdt.setText(stylist.mobile_number)

        }

    }

    fun uploadUserData(editUserPostObj: EditUserPostObj){
        ApiClient.setToken()
        editUserDetailViewModel.uploadUserData(editUserPostObj)
            .observe(this) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->

                                    getProfileData(false, false)
                                }
                            } else {
                                Utils.setErrorData(this, resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(
                                this,
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {

                        }
                    }
                }
            }
    }


    private fun setGenderData(data: ArrayList<String>, view: View) {

        val spinnerAdapter: SelectGenderAdapter = SelectGenderAdapter(view.context, data)
        genderSpinner.adapter = spinnerAdapter

        genderSpinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {

                if(parent.selectedItem == "Male"){
                    selectedGender = "male"
                }else if(parent.selectedItem == "Female"){
                    selectedGender = "female"
                }

                //selectedGender = parent.selectedItem as String

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    fun validateFields(): Boolean {
        fullName = edtName.text.toString()
        emailAdd = emailEdt.text.toString()
        aboutAs = aboutMeEdt.text.toString()
        mobileNumber = mobileNumberEdt.text.toString()


        if (fullName.isNullOrEmpty()) {
            edtName.setError("Full Name can not be empty")
            return false
        }

        if(aboutAs.isNullOrEmpty()){
            aboutMeEdt.setError("AboutUs can not be empty")
            return false
        }

        if (aboutMeEdt?.text?.length?:0 > 251) {
            aboutMeEdt.setError("About length must be less than or equal to 250 characters long")
            return false
        }

        val dtm1 = selectedGender

        if(dtm1.isNullOrEmpty()){
            Utils.showToast(
                this,
                "Please choose gender",
                Toast.LENGTH_SHORT
            )
            return false
        }

        if(birthDateString.isNullOrEmpty()){
            tvBirthdate.setError("Birthdate can not be empty")
            return false
        }

        if(mobileNumber.isNullOrEmpty()) {
            mobileNumberEdt.setError("Mobile can not be empty")
            return false
        }


        if(isValidMobileNumber(mobileNumberEdt.text.toString())){
            if (emailAdd.isNullOrEmpty()) {
                emailEdt.setError("Email can not be empty")
                return false
            } else {
                val emval = isValidEmail(emailAdd)
                if (emval) {
                    return true
                } else {
                    emailEdt.setError("Email is invalid")
                    return false
                }
            }
        }


        return true
    }


    fun isValidMobileNumber(mobileno: String):Boolean{
        val phoneUtil = PhoneNumberUtil.getInstance()
        try {

            val numberProto = phoneUtil.parse(mobileNumber, ccp.selectedCountryNameCode)
            val mobNum =
                phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164)

            if (phoneUtil.getNumberType(numberProto) == PhoneNumberUtil.PhoneNumberType.MOBILE || phoneUtil.getNumberType(
                    numberProto
                ) == PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE
            ) {
                return true
            } else {
                mobileNumberEdt.setError(getString(R.string.err_invalid_number))
                Utils.showToast(
                    this,
                    getString(R.string.err_invalid_number),
                    Toast.LENGTH_SHORT
                )
            }
        } catch (e: NumberParseException){
            mobileNumberEdt.setError(getString(R.string.err_invalid_number))
            return false
        }
        return true
    }


    fun isValidEmail(target: CharSequence?): Boolean {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }
    override fun onBackPressed() {
        super.onBackPressed()
        navBackBtn?.performClick()
    }



    fun uploadImagesfun(selectedImg: Uri) {


        var parts: MultipartBody.Part? = null

        val realpath = RealPathUtil.getRealPath(this, Uri.parse(selectedImg.toString()))
        val file = File(realpath)
        val surveyBody = RequestBody.create("image/jpeg".toMediaTypeOrNull(), file)
         parts = MultipartBody.Part.createFormData("profile_pic", file.name, surveyBody)


        ApiClient.setToken()

        editUserDetailViewModel.uploadUserImage(parts)
                .observe(this) {
                    it?.let { resource ->
                        when (resource.status) {
                            Status.SUCESS -> {

                                if (resource.data?.isSuccessful!!) {
                                    Utils.showToast(
                                        this,
                                        "Successfully Upload Image",
                                        Toast.LENGTH_SHORT
                                    )
                                    getProfileData(false, true)
                                } else {
                                    Utils.setErrorData(this, resource.data.errorBody())
                                }

                            }
                            Status.ERROR -> {

                                Utils.showToast(
                                    this,
                                    it.message.toString(),
                                    Toast.LENGTH_LONG
                                )
                            }
                            Status.LOADING -> {

                            }

                        }
                    }
                }
        }
}