package com.stylist.activities

import android.content.Intent
import android.os.Bundle
import com.stylist.R
import com.stylist.utils.SharedPrefrence
import kotlinx.coroutines.*
import spencerstudios.com.bungeelib.Bungee
import kotlin.coroutines.CoroutineContext

class SplashActivity : BaseActivity(), CoroutineScope {


    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        fullScreen()

        launch {
            delay(2000)
            withContext(Dispatchers.Main) {
                navigateToMainActivity()
            }
        }

    }


    private fun navigateToMainActivity() {
        var isLogin=SharedPrefrence.getLogin(this)
        if(isLogin){
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        }else {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
            finish()
            Bungee.slideLeft(this)
        }
    }


}
