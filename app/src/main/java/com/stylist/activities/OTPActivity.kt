package com.stylist.activities


import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.stylist.R
import com.stylist.SMSBroadcast.SmsVerifyCatcher
import com.stylist.data.LoginResponse
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.CoreConstants
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import com.stylist.viewmodels.LoginViewModel
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.coroutines.*
import spencerstudios.com.bungeelib.Bungee
import java.util.regex.Pattern
import kotlin.coroutines.CoroutineContext


class OTPActivity : AppCompatActivity(), View.OnClickListener, TextView.OnEditorActionListener, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()

    private var mobNum = ""

    private lateinit var loginViewModel: LoginViewModel
    private var otp: String? = ""
    private var cTimer: CountDownTimer? = null
    private var smsVerifyCatcher: SmsVerifyCatcher? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)

        btnOtpVerify.isEnabled = false
        setupViewModel()
        mobNum = intent?.getStringExtra(CoreConstants.Intent.INTENT_MOB_NUMBER)!!


        if (mobNum.isNotEmpty()) {

            tvDescription.text =
                String.format(resources.getString(R.string.txt_description_sms), mobNum)
        }


        smsVerifyCatcher?.setPhoneNumberFilter("VM-DSHSMS")

        smsVerifyCatcher = SmsVerifyCatcher(
            this
        ) { message ->
            val code: String? = parseCode(message)

            //Parse verification code
            codeEdtLogin.setText(code) //set code in edit text
            //then you can send verification code to server
        }



        reverseTimer(60)
        btnOtpVerify.setOnClickListener(this)
        tvResendCode.setOnClickListener(this)

        codeEdtLogin.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                stopAnim()
                otp = codeEdtLogin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    verifyOtp()
                } else {
                    stopAnim()
                }
            }
            false
        }
        codeEdtLogin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                btnOtpVerify.isEnabled = s?.length == 6
            }

        })


    }



    private fun setupViewModel() {
        loginViewModel =
            ViewModelProvider(this, LoginViewModel.ViewModelFactory(ApiClient().apiService))
                .get(LoginViewModel::class.java)
    }

    private fun startAnim() {
        if (avLoading != null) {
            avLoading.show()
            avLoading.visibility = View.VISIBLE
        }

    }

    private fun stopAnim() {
        if (avLoading != null) {
            avLoading.hide()
            avLoading.visibility = View.GONE
        }

    }

    private fun reverseTimer(Seconds: Int) {


        tvResendCode.visibility = View.INVISIBLE
        tvSecond.visibility = View.VISIBLE
        tvSecond.text = "00:00"
        cTimer?.cancel()
        cTimer = object : CountDownTimer((Seconds * 1000 + 1000).toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var seconds = (millisUntilFinished / 1000).toInt()
                val minutes = seconds / 60
                seconds %= 60
                tvSecond.text =
                    String.format("%02d", minutes) + ":" + String.format("%02d", seconds)

            }

            override fun onFinish() {

                tvResendCode.visibility = View.VISIBLE
                tvSecond.visibility = View.GONE
                tvResendCode.text = getString(R.string.txt_resend_code)


            }
        }.start()
    }


    private fun verifyOtp() {
        ApiClient.setToken()
        otp?.let { loginViewModel.verifyOTP(mobNum, it) }?.observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {

                    Status.SUCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                            resource.data.let { response -> response.body()?.data?.let { it1 ->
                                setResponseFromLogin(
                                    it1
                                )
                            } }

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(
                            this@OTPActivity,
                            getString(R.string.error_otp),
                            Toast.LENGTH_SHORT
                        )
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }

            }
        })

    }

    private fun setData(response: LoginResponse?) {
        response?.customToken?.let { SharedPrefrence.setSessionToken(this, it) }
    }
    private fun setResponseFromLogin(loginResponse: LoginResponse) {
        val customToken = loginResponse.customToken
        SharedPrefrence.setSessionToken(this, customToken)
        SharedPrefrence.setLogin(this, true)
        launch {

            withContext(Dispatchers.Main) {
                navigateToMainActivity()
            }
        }

    }

    private fun navigateToMainActivity() {

        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
        Bungee.slideLeft(this)

    }
    private fun getOtp() {
        ApiClient.setToken()
        reverseTimer(60)
        loginViewModel.getOtpbyMob(mobNum, "sms").observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {

                        } else {
                            Utils.setErrorData(this, resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(this, it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Bungee.slideRight(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnOtpVerify -> {
                stopAnim()
                otp = codeEdtLogin.text?.toString()
                if (otp?.isNotEmpty() == true && otp?.length == 6) {
                    cTimer?.cancel()
                    verifyOtp()
                } else {
                    stopAnim()
                }
            }
            R.id.tvResendCode -> {
                getOtp()
            }
        }
    }


    private fun parseCode(message: String): String? {
        val p = Pattern.compile("\\b\\d{6}\\b")
        val m = p.matcher(message)
        var code: String? = ""
        while (m.find()) {
            code = m.group(0)
        }
        return code
    }

    override fun onStart() {
        super.onStart()
        smsVerifyCatcher!!.onStart()
    }

    override fun onStop() {
        super.onStop()
        smsVerifyCatcher!!.onStop()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults!!)
        smsVerifyCatcher!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return false
    }
}