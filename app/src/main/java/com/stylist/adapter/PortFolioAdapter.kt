package com.stylist.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.activities.ImageDetailScreenActivity
import com.stylist.activities.OTPActivity
import com.stylist.data.PortFolioObj
import com.stylist.utils.CoreConstants
import com.stylist.utils.Utils
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 28,January,2021
 */
class PortFolioAdapter(val context: Context,
                       val portfloList: ArrayList<PortFolioObj>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    var isLoading = false

    var loadViewHolder: LoadHolder? = null
    var mListener: PortFolioListInterface? = null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_portfolio_row, parent, false)
            siteAddViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
      /*  if (position == portfloList.size) {
            return LOAD_TYPE
        } else {
            return ROW_TYPE
        }*/
        return ROW_TYPE
    }

    override fun getItemCount(): Int {
        return portfloList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is siteAddViewHolder) {
            val dta = portfloList[position]

            Utils.setImageUsingGlide(context,dta.url,holder.portFlowImg)

            holder.portFlowImg.setOnClickListener {
                mListener?.onListRowClick(dta,position)
            }


        } else if (holder is LoadHolder) {
            loadViewHolder = holder as LoadHolder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }

    }




    inner class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<ProgressBar>(R.id.progressBarReview) as ProgressBar
        }

    }

    internal inner class siteAddViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var portFlowImg: ImageView = view.findViewById<ImageView>(R.id.portflo_rw_img)



    }




    interface PortFolioListInterface{

        fun onListRowClick(portflowObj:PortFolioObj,position: Int)
    }

    fun setListener(listener: PortFolioListInterface) {

        mListener = listener
    }
}