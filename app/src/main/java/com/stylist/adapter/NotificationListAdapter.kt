package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.utils.CoreConstants
import com.stylist.utils.Utils
import com.techniexe.testing.data.notificationObj


class NotificationListAdapter(val context: Context,private var notificationList: ArrayList<notificationObj>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2
    private var notificationClick: NotificationItemClick? = null
    private var loadViewHolder: LoadHolder? = null

    fun addLoadMoreLikes(list: ArrayList<notificationObj>) {
        notificationList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return notificationList.size + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NotificationHolder) {
            val notificationData = notificationList[position]

            var fullString = ""

            if (notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.appointmentReschedule) {
                //  holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.bookAppointmentType

                fullString = String.format(
                    context.getString(
                        R.string.appointment_reschedule_not,
                        notificationData.to_user?.full_name,notificationData?.appointment?.appointment_id?:""
                    )
                )

                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }

            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.appointmentSchedule){
                //  holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.paymentConfirmationType



                fullString = String.format(
                    context.getString(
                        R.string.appointment_schedule_not,
                        notificationData.to_user?.full_name,notificationData.appointment?.appointment_id?:""
                    )
                )
                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }



            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.cancelAppointmentSchedule){
                //  holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.appoitmentApprovedType

                fullString = String.format(
                    context.getString(
                        R.string.cancel_appo_schedule_not,
                        notificationData.to_user?.full_name,notificationData?.appointment?.appointment_id?:""
                    )
                )
                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }


            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.appointmentComplete){
                //  holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.appointmentRescheduleType

                fullString = String.format(
                    context.getString(
                        R.string.appointmentcomplete_not,
                        notificationData.to_user?.full_name,notificationData?.appointment?.appointment_id?:""
                    )
                )
                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }

            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.appointmentOngoing){
                //  holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.appointmentScheduleType

                fullString = String.format(
                    context.getString(
                        R.string.appointment_ongoing,
                        notificationData.to_user?.full_name,notificationData?.appointment?.appointment_id?:""
                    )
                )
                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }


            }else if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.addReview){
                //   holder.notificationMsg.text = CoreConstants.NOTIFICATIONTYPE.cancelAppointmentScheduleType

                fullString = String.format(
                    context.getString(
                        R.string.add_review,
                        notificationData.to_user?.full_name
                    )
                )
                notificationData.to_user?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }


            }else{
                // holder.notificationMsg.text = ""
                fullString = ""
                notificationData.to_user?.full_name?.let {
                    Utils.setSpanColor(
                        context,
                        holder.notificationText,
                        fullString,
                        notificationData.to_user?.full_name?:"",
                        R.color.colorPrimary
                    )
                }

            }


            // holder.notificationText.text = position.toString()+ " TYPE: "+notificationData.notification_type


            val time = Utils.covertTimeToText(notificationData.created_at ?: "")
            holder.notificationTime.text = time

            holder.itemView?.setOnClickListener {


                /*  if(notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.paymentConfirmation ||
                      notificationData.notification_type == CoreConstants.NOTIFICATIONTYPE.orderCancel ){
                          if(notificationData?.order_id != null) {
                              val intent = Intent(context, OrderDetailActivtiy::class.java)
                              intent.putExtra("OrderId", notificationData?.order_id)
                              context.startActivity(intent)
                              Bungee.slideLeft(context)
                          }
                  }else{

                      if(notificationData?.appointment?.appointment_id != null) {

                          val intent = Intent(context, AppointmentDetailActivity::class.java)
                          intent.putExtra("appointment_Id", notificationData?.appointment?.appointment_id)
                          context.startActivity(intent)
                          Bungee.slideLeft(context)
                      }
                  }*/
                notificationData.seen_on = "seenOn"
                notificationClick?.itemClick(notificationData)
                notifyItemChanged(position)

            }

            if (notificationData.seen_on != null) {
                holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            } else {
                holder.itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.notification_30
                    )
                )
            }

        } else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder?.progressBar?.isIndeterminate = true
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification_list, parent, false)
            NotificationHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }

    }


    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar = view.findViewById(R.id.progressBarReview) as ProgressBar

    }


    override fun getItemViewType(position: Int): Int {
        return if (position == notificationList.size) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }

    internal class NotificationHolder(view: View) : RecyclerView.ViewHolder(view) {

        //   var notificationMsg: TextView = view.findViewById(R.id.notificationMsg)
        var notificationTime: TextView = view.findViewById(R.id.notificationTime)
        var notificationText: TextView = view.findViewById(R.id.notificationText)

    }

    fun loading() {
        loadViewHolder?.progressBar?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder?.progressBar?.visibility = View.GONE
    }

    interface NotificationItemClick {
        fun itemClick(notificationdta: notificationObj)
    }

    fun notificationItemClick(notificationItemClick: NotificationItemClick) {
        this.notificationClick = notificationItemClick
    }

}