package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.stylist.R
import kotlinx.android.synthetic.main.gender_spinner_row.view.*


/**
 * Created by Hitesh Patel on 17,February,2021
 */
class SelectGenderAdapter(ctx: Context, moods: ArrayList<String>?) :
    ArrayAdapter<String>(ctx, 0, moods!!) {
    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {
        val stateModel = getItem(position)
        val view = recycledView ?: LayoutInflater.from(context).inflate(
            R.layout.gender_spinner_row,
            parent,
            false
        )
        view.title_spinner_item.text = stateModel?.toString()

        return view
    }
}