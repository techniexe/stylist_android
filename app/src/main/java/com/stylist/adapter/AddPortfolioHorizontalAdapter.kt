package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions


import com.stylist.R
import com.stylist.data.Image


/**
 * Created by Hitesh Patel on 15,February,2021
 */
class AddPortfolioHorizontalAdapter (val context: Context, val listener:
AskAndSharePhotoAdapterListener, val selectedImages:ArrayList<Image>) :
        RecyclerView
        .Adapter<RecyclerView.ViewHolder>(){



    init {

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is AskAndSharePhotoHolder) {
            if (position == 0) {
                holder.imgAsk.visibility = View.GONE
                holder.deleteImg.visibility = View.GONE
                holder.cameraIcon.visibility = View.VISIBLE
                holder.backgroundImage.visibility = View.VISIBLE

                holder.backgroundImage.setOnClickListener {
                    listener.onCameraClicked()
                }

            } else {
                val image = selectedImages.get(position-1)
                val mediaPath = image.getPath()
                holder.imgAsk.visibility = View.VISIBLE
                holder.deleteImg.visibility = View.VISIBLE
                holder.cameraIcon.visibility = View.GONE
                holder.backgroundImage.visibility = View.GONE
                holder.deleteImg.setOnClickListener {
                    listener.onDeleteClick(position-1)
                }

                val requestOption = RequestOptions().placeholder(R.drawable.placeholder).error(R.drawable.placeholder).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate()
                Glide.with(context).load(mediaPath)
                        .apply(requestOption)
                        //.transition(DrawableTransitionOptions.withCrossFade())
                        .into(holder.imgAsk)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_pic_portfolio, parent, false)
        //  mBannerAdapterHelper.onCreateViewHolder(parent, view);
        return AskAndSharePhotoHolder(view)
    }

    override fun getItemCount(): Int {
        return selectedImages.size+1
    }

    interface AskAndSharePhotoAdapterListener {
        fun onCameraClicked()
        fun onDeleteClick(position: Int)
    }

    internal inner class AskAndSharePhotoHolder(view: View) : RecyclerView.ViewHolder(view) {
        var backgroundImage: FrameLayout
        var cameraIcon: ImageView
        var imgAsk: ImageView
        var deleteImg: ImageView

        init {
            backgroundImage = view.findViewById(R.id.backgroundImage)
            cameraIcon = view.findViewById(R.id.cameraIcon)
            imgAsk = view.findViewById(R.id.imgAsk)
            deleteImg = view.findViewById(R.id.deleteImg)
        }
    }
}