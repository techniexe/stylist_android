package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.data.FeedBackDetail
import com.stylist.utils.Utils
import kotlinx.android.synthetic.main.row_load.view.*


class ReviewdapterWithPaging(var context: Context, private var reviewList: ArrayList<FeedBackDetail>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2

    private var loadViewHolder: LoadHolder? = null


    fun addLoadMoreLikes(list: ArrayList<FeedBackDetail>) {
        reviewList=list
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return reviewList.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_review, parent, false)
            CartItemViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == reviewList.size) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }
    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var progressBar: ProgressBar = view.findViewById(R.id.progressBarReview) as ProgressBar

    }



    fun loading() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.GONE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val review=reviewList[position]


            holder.tvName.text=review.user_full_name

            holder.tvDescription.text=review.text
           // Utils.setImageUsingGlide(context,review?.image_url,holder.imgCategory)

           val time = Utils.covertTimeToText(review.created_at ?: "")
            holder.tvTime.text = time
            holder.ratingBar.rating= review.rating!!.toFloat()


        }
        else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder!!.itemView.progressBarReview?.isIndeterminate = true
        }
    }

    internal  class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvTime: TextView = view.findViewById(R.id.tvTime)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var ratingBar: RatingBar = view.findViewById(R.id.ratting)

    }


}
