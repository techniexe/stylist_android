package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.data.AppointmentDetails
import com.stylist.utils.Utils
import kotlinx.android.synthetic.main.item_appointment.view.*


class Appointmentdapter(var context: Context, private var appointment: ArrayList<AppointmentDetails>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int {
        return appointment.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_appointment, parent, false)
        return CartItemViewHolder(view)

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val appointment = appointment[position]


                if(appointment.service!=null) {
                    holder.itemView.tvName.text = appointment.service.name

                    holder.itemView.tvDate.text=  Utils.getConvertDate(appointment.start_time ?: "")

                    holder.itemView.btnTime.setText(Utils.getTime(appointment.start_time?:""))
                    holder.itemView.tvClientName.text = appointment.client?.full_name?:""
                    //holder.itemView.tvStatusTxt.text = appointment.appointment_status

                    Utils.AppointmentStatusUpdateUi(appointment.appointment_status,holder.itemView.tvStatusTxt,context)


            }
        }

    }

    internal class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {


    }


}
