package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.data.HolidayResListData
import com.stylist.utils.Utils
import kotlinx.android.synthetic.main.row_load.view.*


/**
 * Created by Hitesh Patel on 08,February,2021
 */
class HolidayAdapterWithPaging (var context: Context, private var holidayList: ArrayList<HolidayResListData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2

    private var loadViewHolder: LoadHolder? = null


    fun addLoadMoreLikes(list: ArrayList<HolidayResListData>) {
        holidayList=list
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return holidayList.size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_holiday_lst_row, parent, false)
            HolidayItemViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == holidayList.size) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }
    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var progressBar: ProgressBar = view.findViewById(R.id.progressBarReview) as ProgressBar

    }



    fun loading() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.GONE
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is HolidayItemViewHolder) {
            val holiday=holidayList[position]
            holder.tvName.text=holiday.branch?.name

            val time = Utils.getConvertDate(holiday.date ?: "")
            holder.tvDate.text = time

            holder.tvholidayName.text = holiday.holiday_name



        }
        else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder!!.itemView.progressBarReview?.isIndeterminate = true
        }
    }

    internal  class HolidayItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvDate: TextView = view.findViewById(R.id.tvDate)
        var tvholidayName:TextView = view.findViewById(R.id.tvholidayName)


    }
}