package com.stylist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.stylist.R
import com.stylist.data.FeedBackDetail
import com.stylist.utils.Utils


class Reviewdapter(var context: Context, private var reviewList: ArrayList<FeedBackDetail>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int {
        return reviewList.size

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_review, parent, false)
        return CartItemViewHolder(view)

    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val review = reviewList[position]

            holder.tvName.text = review.user_full_name

            holder.tvDescription.text = review.text

            Utils.setImageUsingGlide(
                context,
                review.user_profile_pic,
                holder.imgView
            )

            val time = Utils.covertTimeToText(review.created_at?:"")
            holder.tvTime.text = time

            holder.ratingBar.rating = review.rating!!.toFloat()

        }

    }

    internal class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvName: TextView = view.findViewById(R.id.tvName)
        var tvTime: TextView = view.findViewById(R.id.tvTime)
        var tvDescription: TextView = view.findViewById(R.id.tvDescription)
        var ratingBar: RatingBar = view.findViewById(R.id.ratting)
        var imgView: ImageView = view.findViewById(R.id.imgProfile)

    }


}
