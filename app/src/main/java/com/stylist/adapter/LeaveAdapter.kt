package com.stylist.adapter

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.alexandrius.accordionswipelayout.library.SwipeLayout
import com.alexandrius.accordionswipelayout.library.SwipeLayout.OnSwipeItemClickListener
import com.stylist.R
import com.stylist.data.LeaveList
import com.stylist.dialog.AddLeaveDialog
import com.stylist.fragments.ApplyLeaveFragment
import com.stylist.utils.CoreConstants
import com.stylist.utils.Utils
import kotlinx.android.synthetic.main.row_load.view.*
import spencerstudios.com.bungeelib.Bungee


class LeaveAdapter(
    var applyLeaveFragment: ApplyLeaveFragment, private var leaveList
    : ArrayList<LeaveList>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val ROW_TYPE = 1
    private val LOAD_TYPE = 2


    private var loadViewHolder: LoadHolder? = null


    fun addLoadMoreLikes(list: ArrayList<LeaveList>) {
        leaveList = list
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return leaveList
            .size + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        return if (viewType == ROW_TYPE) {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_item, parent, false)
            CartItemViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.row_load, parent, false)
            LoadHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == leaveList
                .size
        ) {
            LOAD_TYPE
        } else {
            ROW_TYPE
        }
    }

    class LoadHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var progressBar: ProgressBar =
            view.findViewById(R.id.progressBarReview) as ProgressBar

    }


    fun loading() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.VISIBLE
    }

    fun loadDone() {
        loadViewHolder!!.itemView.progressBarReview?.visibility = View.GONE
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder) {
            val leaveList = leaveList[position]


            if(leaveList.leave_status==CoreConstants.STATUS.APPROVED){
                (holder.itemView as SwipeLayout).isSwipeEnabled = false
                holder.tvStatus.setBackgroundColor(ContextCompat.getColor(applyLeaveFragment.requireContext(), R.color.colorGreen));
            }else if(leaveList.leave_status==CoreConstants.STATUS.UNAPPROVED){
                holder.tvStatus.setBackgroundColor(ContextCompat.getColor(applyLeaveFragment.requireContext(), R.color.colorPrimary));
                (holder.itemView as SwipeLayout).isSwipeEnabled = true
            }else if(leaveList.leave_status==CoreConstants.STATUS.REJECTED){
                (holder.itemView as SwipeLayout).isSwipeEnabled = false
                holder.tvStatus.setBackgroundColor(ContextCompat.getColor(applyLeaveFragment.requireContext(), R.color.colorHoliday_txt));
            }else if(leaveList.leave_status==CoreConstants.STATUS.CANCELLED){
                (holder.itemView as SwipeLayout).isSwipeEnabled = false
                holder.tvStatus.setBackgroundColor(ContextCompat.getColor(applyLeaveFragment.requireContext(), R.color.colorHoliday_txt));
            }
            holder.tvStatus.text = leaveList.leave_status
            holder.tvName.text = leaveList.reason
            val time = String.format(
                applyLeaveFragment.getString(R.string.txt_applyon),
                Utils.getConvertDate(leaveList.created_at ?: "")
            )
            holder.tvAppliedOn.text = time

            if (!leaveList.start_time.isNullOrEmpty() && !leaveList.end_time.isNullOrEmpty()) {
                var sDate = String.format(
                    applyLeaveFragment.getString(R.string.txt_sdate), Utils.getConvertDate(
                        leaveList.start_time
                    )
                )
                Utils.setSpanColor(
                    applyLeaveFragment.requireContext(),
                    holder.tvStartDate,
                    sDate,
                    applyLeaveFragment.getString(R.string.txt_s_date),
                    R.color.colorLGrey
                )

                var eDate = String.format(
                    applyLeaveFragment.getString(R.string.txt_edate), Utils.getConvertDate(
                        leaveList.end_time
                    )
                )
                Utils.setSpanColor(
                    applyLeaveFragment.requireContext(),
                    holder.tvEDate,
                    eDate,
                    applyLeaveFragment.getString(R.string.txt_e_date),
                    R.color.colorLGrey
                )
            }


           //     (holder.itemView as SwipeLayout).animation.reset()
                holder.itemView.tag = position

        } else if (holder is LoadHolder) {
            loadViewHolder = holder
            loadViewHolder!!.itemView.progressBarReview?.isIndeterminate = true
        }
    }

    inner class CartItemViewHolder(view: View) : RecyclerView.ViewHolder(view),
        View.OnClickListener, OnSwipeItemClickListener {


        init {
            itemView.setOnClickListener(this)


            (itemView as SwipeLayout).setOnSwipeItemClickListener(this)
        }

        var tvName: TextView = view.findViewById(R.id.tvDescription)
        var tvStatus: TextView = view.findViewById(R.id.tvStatus)
        var tvAppliedOn: TextView = view.findViewById(R.id.tvAppliedOn)
        var tvStartDate: TextView = view.findViewById(R.id.tvStartDate)
        var tvEDate: TextView = view.findViewById(R.id.tvEndDate)

        override fun onClick(v: View?) {

            when (v?.id) {


            }
        }


        override fun onSwipeItemClick(left: Boolean, index: Int) {
            if (left) {

            } else {
                when (index) {
                    0 -> {
                        (itemView as SwipeLayout).collapseAll(true)
                        var addLeaveDialog = AddLeaveDialog(leaveList[adapterPosition],adapterPosition)
                        addLeaveDialog.setListener(applyLeaveFragment)
                        addLeaveDialog.show(
                            (applyLeaveFragment.childFragmentManager),
                            "leaveDialog"
                        )
                        Bungee.slideUp(applyLeaveFragment.requireContext())
                    }
                    1 -> {
                      applyLeaveFragment.dialogDelete(leaveList[adapterPosition]._id)



                    }

                }
            }
        }


    }


}
