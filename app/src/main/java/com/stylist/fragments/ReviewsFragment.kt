package com.stylist.fragments

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.stylist.R
import com.stylist.adapter.ReviewdapterWithPaging
import com.stylist.data.FeedBackDetail
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_review.*
import kotlinx.android.synthetic.main.no_review_plain.*
import kotlinx.coroutines.*
import java.lang.Runnable
import kotlin.coroutines.CoroutineContext


class ReviewsFragment :Fragment() , CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + Job()
    var reviewList = ArrayList<FeedBackDetail>()
    private lateinit var userViewModel: UserViewModel
    var reviewdapter: ReviewdapterWithPaging? = null
    var beforeTime: String? = null
    var isLoading = true
    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_review, container, false)
    }


    override fun onResume() {
        super.onResume()
        setupViewModel()
        getFeedBack(beforeTime)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvReviews.isNestedScrollingEnabled = false;
        nScrolReview.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (oldScrollY > 0) {

                     val visibleItemCount = rvReviews?.layoutManager?.childCount
                     val totalItemCount = rvReviews?.layoutManager?.itemCount
                     val pastVisiblesItems =
                         (rvReviews?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                     if (isLoading) {

                         if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                             isLoading = false
                             if (reviewdapter!=null) {

                                 reviewdapter!!.loading()
                                 beforeTime = reviewList[reviewList.size - 1].created_at

                                 val handler = Handler()
                                 handler.postDelayed(Runnable {

                                     getFeedBack(beforeTime)


                                 }, 500)


                             }
                         }
                     }
                 }
             })


    }


    private fun setupViewModel() {

        userViewModel =
            ViewModelProvider(this, UserViewModel.ViewModelFactory(ApiClient().apiService))
                .get(UserViewModel::class.java)
    }
    private fun getFeedBack(beforeTime: String?) {
        ApiClient.setToken()
        userViewModel.getFeedback(beforeTime).observe(requireActivity()) {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { it1 -> setUpReviewUI(it1.body()?.data) }
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {

                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {


                    }

                }
            }
        }
    }
    private fun setUpReviewUI(rvList: ArrayList<FeedBackDetail>?) {
        launch {

            withContext(Dispatchers.Main) {
                if (rvList.isNullOrEmpty() && reviewList.isNullOrEmpty()) {
                    rvReviews.visibility = View.GONE
                    constrainEmptyOrder.visibility = View.VISIBLE
                } else {
                    rvReviews.visibility = View.VISIBLE
                    constrainEmptyOrder.visibility = View.GONE
                    if (reviewdapter == null) {
                        if (rvList != null) {
                            reviewList.addAll(rvList)
                        }
                        reviewdapter = ReviewdapterWithPaging(requireContext(), reviewList)
                        rvReviews?.adapter = reviewdapter
                    } else {
                        if (rvList != null) {
                            reviewList.addAll(rvList)
                            reviewdapter?.addLoadMoreLikes(reviewList)

                        }
                        isLoading = true
                        reviewdapter?.loadDone()
                    }

                }
            }
        }
    }
    }



