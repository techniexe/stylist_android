package com.stylist.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup

import android.widget.ImageView
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.android.material.tabs.TabLayout
import com.stylist.R
import com.stylist.activities.EditProfileActivity
import com.stylist.activities.ImageDetailScreenActivity

import com.stylist.utils.CoreConstants
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils

import kotlinx.android.synthetic.main.fragment_profile.*
import spencerstudios.com.bungeelib.Bungee


class ProfileFragment : Fragment(), TabLayout.OnTabSelectedListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        tabs.addTab(tabs.newTab().setText(CoreConstants.ProfileTab.ABOUT))
        tabs.addTab(tabs.newTab().setText(CoreConstants.ProfileTab.PROTFOLIO))
        tabs.addTab(tabs.newTab().setText(CoreConstants.ProfileTab.REVIEWS))
        tabs.addTab(tabs.newTab().setText(CoreConstants.ProfileTab.APPLYLEAVES))
        tabs.addTab(tabs.newTab().setText(CoreConstants.ProfileTab.HOLIDAYS))
        tabs.tabGravity = TabLayout.GRAVITY_FILL

        childFragmentManager.commit {
            addToBackStack("AboutUs")
            replace(R.id.profilecontent, AboutUsFragment())
        }

        tabs.addOnTabSelectedListener(this)


        activity?.findViewById<ImageView>(R.id.profile_more_menu)?.setOnClickListener {
            showPopupMenu(activity?.findViewById<ImageView>(R.id.profile_more_menu)!!)
        }


        imgUserProfile?.setOnClickListener {
            val intent = Intent(context, ImageDetailScreenActivity::class.java)
            intent.putExtra("imageUrl", SharedPrefrence.getUserProfile(requireContext()))
            intent.putExtra("deleted", true)
            startActivity(intent)
            Bungee.slideLeft(context)
        }

        tvUserEmail?.setOnClickListener {
            val packageName = "com.google.android.apps.maps"
            //   val query = "google.navigation:q=" + userData?.exploring_next_location?.coordinates!![1] + "," + userData?.exploring_next_location?.coordinates!![0]
            val lat = SharedPrefrence.getBranchLat(requireContext())?:0.0.toDouble()
            val lng = SharedPrefrence.getBranchLong(requireContext())?:0.0.toDouble()
            val query =
                "http://maps.google.com/maps?q=loc:${lat},${lng}"
            val intent =
                requireContext().getPackageManager()?.getLaunchIntentForPackage(packageName)
            intent?.setAction(Intent.ACTION_VIEW)
            intent?.setData(Uri.parse(query))
            requireContext().startActivity(intent)
        }

        /* val intent = Intent(context, ImageDetailScreenActivity::class.java)
         intent.putExtra("imageUrl",portflowObj.url)
         intent.putExtra("position",position)
         intent.putExtra("obj",portflowObj)
         startActivityForResult(intent,240)
         Bungee.slideLeft(context)*/

    }


    override fun onResume() {
        super.onResume()
        setProfileData()

    }

    fun showPopupMenu(v: View) {
        val popup = PopupMenu(requireContext(), v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.edit_profile_main_menu, popup.menu)

        popup.show()
        popup.setOnMenuItemClickListener { item ->

            when (item.itemId) {
                R.id.edit_profile_menu_btn -> {
                    val intent = Intent(requireContext(), EditProfileActivity::class.java)
                    startActivity(intent)
                    Bungee.slideLeft(requireContext())


                    true
                }

                else -> false
            }

        }
    }

    private fun setProfileData() {
        var name = SharedPrefrence.getUserName(requireContext())

        if (!name.isNullOrEmpty())
            tvUsername.text = name

        var designation = SharedPrefrence.getDesignation(requireContext())
        var branch = SharedPrefrence.getBrnachName(requireContext())
        if (!designation.isNullOrEmpty() && !branch.isNullOrEmpty()) {
            var buffer = StringBuffer()
            buffer.append(branch)
            buffer.append(" | ")
            buffer.append(designation)
            tvUserEmail.text = buffer.toString()
        }


        var profile = SharedPrefrence.getUserProfile(requireContext())
        if (!profile.isNullOrEmpty())
            Utils.setImageUsingGlide(requireContext(), profile, imgUserProfile)
    }


    override fun onTabSelected(tab: TabLayout.Tab?) {
        val pos = tab?.position
        when (pos) {
            0 -> {
                var aboutUsFragment = requireActivity().supportFragmentManager.findFragmentByTag(CoreConstants.ProfileTab.ABOUT)
                if (aboutUsFragment == null) {
                    aboutUsFragment = AboutUsFragment()
                }
                replaceFragmentWithBackStack(R.id.profilecontent, aboutUsFragment)
                fab.visibility = View.GONE
            }
            1 -> {
                var portfolioFragment = requireActivity().supportFragmentManager.findFragmentByTag(CoreConstants.ProfileTab.PROTFOLIO)
                if (portfolioFragment == null) {
                    portfolioFragment = PortfolioFragment()
                }
                replaceFragmentWithBackStack(R.id.profilecontent, portfolioFragment)
                fab.visibility = View.VISIBLE
            }
            2 -> {
                var reviewsFragment = requireActivity().supportFragmentManager.findFragmentByTag(CoreConstants.ProfileTab.REVIEWS)
                if (reviewsFragment == null) {
                    reviewsFragment = ReviewsFragment()
                }
                replaceFragmentWithBackStack(R.id.profilecontent, reviewsFragment)
                fab.visibility = View.GONE
            }
            3 -> {
                var applyLeaveFragment = requireActivity().supportFragmentManager.findFragmentByTag(CoreConstants.ProfileTab.APPLYLEAVES)
                if (applyLeaveFragment == null) {
                    applyLeaveFragment = ApplyLeaveFragment()
                }
                replaceFragmentWithBackStack(R.id.profilecontent, applyLeaveFragment)
                fab.visibility = View.VISIBLE
            }
            4 -> {
                var holidaysFragment = requireActivity().supportFragmentManager.findFragmentByTag(CoreConstants.ProfileTab.HOLIDAYS)
                if (holidaysFragment == null) {
                    holidaysFragment = HolidaysFragment()
                }
                replaceFragmentWithBackStack(R.id.profilecontent, holidaysFragment)
                fab.visibility = View.GONE
            }
        }
    }

    override fun onTabUnselected(tab: TabLayout.Tab?) {

    }

    override fun onTabReselected(tab: TabLayout.Tab?) {

    }

    fun replaceFragmentWithBackStack(containerId: Int, fragment: Fragment) {


        childFragmentManager
                .beginTransaction()
                .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
                .replace(containerId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
                .commitAllowingStateLoss()

    }


}



