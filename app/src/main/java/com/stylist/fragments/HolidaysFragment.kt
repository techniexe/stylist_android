package com.stylist.fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.stylist.R
import com.stylist.adapter.HolidayAdapterWithPaging
import com.stylist.data.HolidayResListData
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.HolidayViewModel
import com.whiteelephant.monthpicker.MonthPickerDialog
import kotlinx.android.synthetic.main.fragment_apply_leave.*
import kotlinx.android.synthetic.main.fragment_holidays.*
import kotlinx.android.synthetic.main.fragment_holidays.btnClearAll
import kotlinx.android.synthetic.main.fragment_holidays.imgCalender
import kotlinx.android.synthetic.main.fragment_holidays.llFilter
import kotlinx.android.synthetic.main.fragment_holidays.tvDate
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class HolidaysFragment :Fragment(),View.OnClickListener {

    var beforeTime: String? = null
    var isLoading = true
    private lateinit var holidayViewModel: HolidayViewModel
    var holidayList= ArrayList<HolidayResListData>()
    var holidayAdapter: HolidayAdapterWithPaging? = null
    var selectedMnt:String? = null
    var selectYer:String? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_holidays, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setCurrentDay()
        imgCalender.setOnClickListener(this)
        btnClearAll.setOnClickListener(this)


        rcHoliday.isNestedScrollingEnabled = false;
        nScrolHoliday.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (oldScrollY > 0) {


                    val visibleItemCount = rcHoliday?.layoutManager?.childCount
                    val totalItemCount = rcHoliday?.layoutManager?.itemCount
                    val pastVisiblesItems =
                        (rcHoliday?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                    if (isLoading) {

                        if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                            isLoading = false
                            if (holidayAdapter != null) {

                                holidayAdapter!!.loading()
                                beforeTime = holidayList[holidayList.size - 1].created_at

                                val handler = Handler()
                                handler.postDelayed(Runnable {

                                    getLeave(beforeTime, selectedMnt, selectYer,false)

                                }, 500)
                            }
                        }
                    }
                }
            })


    }


    private fun setNormalPicker() {

        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(
            requireActivity(), { selectedMonth, selectedYear ->


            tvDate.text = getMonth(selectedMonth) + " - " +selectedYear


            selectedMnt = Utils.getSelectedMonthCode(selectedMonth)
            selectYer = selectedYear.toString()

            holidayList.clear()
            holidayAdapter = null

            getLeave(beforeTime, selectedMnt,selectYer,true)
                btnClearAll.visibility=View.VISIBLE
                imgCalender.visibility=View.GONE

            }, today[Calendar.YEAR], today[Calendar.MONTH]
        )
        builder
                //.setActivatedMonth(Calendar.FEBRUARY)
            .setMinYear(1990)
            //.setActivatedYear(Calendar.YEAR)
            .setMaxYear(2030)
            //.setMinMonth(Calendar.JANUARY)
            .setTitle("Select Month")
           // .setMonthRange(Calendar.FEBRUARY, Calendar.DECEMBER)
            // .setMaxMonth(Calendar.OCTOBER)
            // .setYearRange(1890, 1890)
            // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
            //.showMonthOnly()
            // .showYearOnly()
            /*.setOnMonthChangedListener(MonthPickerDialog.OnMonthChangedListener() {
                @Override
                fun onMonthChanged(selectedMonth:Int) {
                    Log.d("TAG", "Selected month : " +  selectedMonth);

                    selectedMnt = Utils.getSelectedMonthCode(selectedMonth)

                    holidayList.clear()
                    holidayAdapter = null

                    getLeave(beforeTime, selectedMnt,selectYer)

                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
            })*/
                .setOnMonthChangedListener { selectedMonth ->

                    Log.d("TAG", "Selected month : " +  selectedMonth);

                    selectedMnt = Utils.getSelectedMonthCode(selectedMonth)

                   /* holidayList.clear()
                    holidayAdapter = null

                    getLeave(beforeTime, selectedMnt,selectYer)*/

                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
            .setOnYearChangedListener { selectedYear ->
                Log.d("TAG", "Selected year : $selectedYear")
                // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();

                selectYer = selectedYear.toString()
               /* holidayList.clear()
                holidayAdapter = null
                getLeave(beforeTime, selectedMnt,selectYer)*/
            }
            .build()
            .show()

    }

    fun getMonth(month: Int): String? {
        return DateFormatSymbols().getMonths().get(month)
    }
    override fun onResume() {
        super.onResume()
        setupViewModel()
        getLeave(beforeTime,selectedMnt,selectYer,false)
    }

    private fun setupViewModel() {
        holidayViewModel =
            ViewModelProvider(this, HolidayViewModel.ViewModelFactory(ApiClient().apiService))
                .get(HolidayViewModel::class.java)
    }

    private fun getLeave(beforeTime: String?,selectmnt:String?,selectYear:String?, isFromFilter: Boolean) {
        ApiClient.setToken()
        holidayViewModel.getHoliday(selectmnt,selectYear,beforeTime,null,null).observe(requireActivity()) {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { it1 ->
                                setUpHolidayUI(it1.body()?.data,isFromFilter)
                            }
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {

                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {


                    }

                }
            }
        }
    }


    private fun setUpHolidayUI(rvList: ArrayList<HolidayResListData>?, isFromFilter: Boolean) {


        if (rvList.isNullOrEmpty() && holidayList.isNullOrEmpty()) {
            rcHoliday.visibility = View.GONE
            emptyviewHoliday.visibility = View.VISIBLE

            if (isFromFilter) {
                btnClearAll.visibility=View.VISIBLE
                imgCalender.visibility=View.GONE
                llFilter.visibility = View.VISIBLE
                var msg = getString(R.string.txt_no_date)
                tvNoData.text = msg

            } else {
                llFilter.visibility = View.GONE
                tvNoData.text = getString(R.string.txt_empty_leave_list)

            }

        } else {
            rcHoliday.visibility = View.VISIBLE
            llFilter.visibility=View.VISIBLE
            emptyviewHoliday.visibility = View.GONE
            if (holidayAdapter == null) {
                if (rvList != null) {
                    holidayList
                            .addAll(rvList)
                }
                holidayAdapter = HolidayAdapterWithPaging(requireContext(), holidayList)
                rcHoliday?.adapter = holidayAdapter
            } else {
                if (rvList != null) {
                    holidayList.addAll(rvList)
                    holidayAdapter?.addLoadMoreLikes(holidayList)

                }
                isLoading = true
                holidayAdapter?.loadDone()
            }
        }


    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnClearAll -> {
                setCurrentDay()
                btnClearAll.visibility=View.GONE
                imgCalender.visibility=View.VISIBLE
                holidayList.clear()
                selectedMnt = null
                selectYer = null
                beforeTime = null
                getLeave(beforeTime, selectedMnt,selectYer,false)
            }
            R.id.imgCalender -> {
                setNormalPicker()

            }

        }
    }
    private fun setCurrentDay(){
        var calender=Calendar.getInstance()
        var formate= SimpleDateFormat("MMMM - yyyy")
        tvDate.setText(formate.format(calender.time))
    }
}