package com.stylist.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.stylist.R
import com.stylist.adapter.LeaveAdapter
import com.stylist.data.AddLeave
import com.stylist.data.EditLeaveResponse
import com.stylist.data.LeaveDelete
import com.stylist.data.LeaveList
import com.stylist.dialog.AddLeaveDialog
import com.stylist.dialog.BreakAlertDialog
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.LeaveViewModel
import com.whiteelephant.monthpicker.MonthPickerDialog
import kotlinx.android.synthetic.main.fragment_apply_leave.*
import kotlinx.android.synthetic.main.fragment_profile.*
import spencerstudios.com.bungeelib.Bungee
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ApplyLeaveFragment : Fragment(), View.OnClickListener, AddLeaveDialog.DialogToFragment {

    var selectedMnt: String? = null
    var selectYer: String? = null
    var beforeTime: String? = null
    var isLoading = true
    private lateinit var leaveViewModel: LeaveViewModel
    var leaveList = ArrayList<LeaveList>()
    var leaveAdapter: LeaveAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_apply_leave, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setCurrentDay()
        imgCalender.setOnClickListener(this)
        btnClearAll.setOnClickListener(this)
        val parentFragment = parentFragment
        (parentFragment as ProfileFragment).fab.setOnClickListener(this)


        rcLeave.isNestedScrollingEnabled = false;
        nScrollLeave.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (oldScrollY > 0) {

                val visibleItemCount = rcLeave?.layoutManager?.childCount
                val totalItemCount = rcLeave?.layoutManager?.itemCount
                val pastVisiblesItems =
                    (rcLeave?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                if (isLoading) {

                    if (visibleItemCount?.plus(pastVisiblesItems)!! >= totalItemCount ?: 0) {
                        isLoading = false
                        if (leaveAdapter != null) {

                            leaveAdapter!!.loading()
                            beforeTime = leaveList[leaveList.size - 1].created_at

                            val handler = Handler()
                            handler.postDelayed(Runnable {

                                getLeave(beforeTime, selectedMnt, selectYer, false)


                            }, 500)


                            }
                        }
                    }
                }
            })


    }


    private fun setNormalPicker() {

        val today = Calendar.getInstance()
        val builder = MonthPickerDialog.Builder(
            requireActivity(), { selectedMonth, selectedYear ->


                tvDate.text = getMonth(selectedMonth) + " - " + selectedYear


                selectedMnt = Utils.getSelectedMonthCode(selectedMonth)
                selectYer = selectedYear.toString()

                leaveList.clear()
                leaveAdapter = null

                getLeave(beforeTime, selectedMnt, selectYer, true)
                btnClearAll.visibility=View.VISIBLE
                imgCalender.visibility=View.GONE

            }, today[Calendar.YEAR], today[Calendar.MONTH]
        )
        builder.setActivatedMonth(Calendar.getInstance().get(Calendar.MONTH))
            .setMinYear(1990)
            .setActivatedYear(Calendar.getInstance().get(Calendar.YEAR))
            .setMaxYear(2030)
            //.setMinMonth(Calendar.JANUARY)
            .setTitle("Select Year & Month")
            //.setMonthRange(Calendar.JANUARY, Calendar.DECEMBER)
            // .setMaxMonth(Calendar.OCTOBER)
            // .setYearRange(1890, 1890)
            // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
            //.showMonthOnly()
            // .showYearOnly()

            .setOnMonthChangedListener(MonthPickerDialog.OnMonthChangedListener {
                @Override
                fun onMonthChanged(selectedMonth: Int) {
                    Log.d("TAG", "Selected month : " + getMonth(selectedMonth))

                    // Toast.makeText(MainActivity.this, " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                }
            })

            .setOnYearChangedListener { selectedYear ->
                Log.d("TAG", "Selected year : $selectedYear")
                // Toast.makeText(MainActivity.this, " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
            }

            .setOnMonthChangedListener {  }
            .build()
            .show()

    }

    fun getMonth(month: Int): String? {
        return DateFormatSymbols().months.get(month)
    }

    override fun onResume() {
        super.onResume()
        setupViewModel()
        getLeave(beforeTime, selectedMnt, selectYer, false)
    }

    private fun setupViewModel() {
        leaveViewModel =
            ViewModelProvider(this, LeaveViewModel.ViewModelFactory(ApiClient().apiService))
                .get(LeaveViewModel::class.java)
    }

    private fun getLeave(
        beforeTime: String?,
        selectmnt: String?,
        selectYear: String?,
        isFromFilter: Boolean
    ) {
        ApiClient.setToken()
        leaveViewModel.getLeave(selectmnt, selectYear, beforeTime, null)
            .observe(requireActivity()) {
                it.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let { it1 ->
                                    setUpReviewUI(it1.body()?.data, isFromFilter)
                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(
                                requireContext(),
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {


                        }

                    }
                }
            }
    }

    private fun setUpReviewUI(rvList: ArrayList<LeaveList>?, isFromFilter: Boolean) {
        if (rvList.isNullOrEmpty() && leaveList.isNullOrEmpty()) {
            rcLeave.visibility = View.GONE
            constrainEmptyOrder.visibility = View.VISIBLE

            if (isFromFilter) {
                btnClearAll.visibility=View.VISIBLE
                imgCalender.visibility=View.GONE
                llFilter.visibility = View.VISIBLE
                var msg = getString(R.string.txt_no_date)
                tvNoData.text = msg

            } else {
                llFilter.visibility = View.GONE
                tvNoData.text = getString(R.string.txt_empty_leave_list)

            }

        } else {
            llFilter.visibility = View.VISIBLE
            rcLeave.visibility = View.VISIBLE
            constrainEmptyOrder.visibility = View.GONE
            if (leaveAdapter == null) {

                if (rvList != null) {
                    leaveList.addAll(rvList)
                }


                leaveAdapter = LeaveAdapter(this, leaveList)
                rcLeave?.adapter = leaveAdapter

            } else {

                if (rvList != null) {
                    leaveList.addAll(rvList)
                    leaveAdapter?.addLoadMoreLikes(leaveList)
                }



                isLoading = true
                leaveAdapter?.loadDone()
            }
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnClearAll -> {
                setCurrentDay()
                btnClearAll.visibility=View.GONE
               imgCalender.visibility=View.VISIBLE
                leaveList.clear()
                selectedMnt = null
                selectYer = null
                beforeTime = null
                getLeave(beforeTime, selectedMnt, selectYer, false)
            }
            R.id.imgCalender -> {

                setNormalPicker()

            }
            R.id.fab -> {

                var addLeaveDialog = AddLeaveDialog(null, 0)
                addLeaveDialog.setListener(this)
                addLeaveDialog.show(requireActivity().supportFragmentManager, "leaveDialog")
                Bungee.slideUp(requireContext())

            }
        }
    }

    override fun dialogDismiss() {

    }

    override fun dialogSaveAddEdit(addLeave: AddLeave, leaveId: String?, adapterPosition: Int) {
        ApiClient.setToken()
        if (leaveId != null) {
            leaveViewModel.editleave(addLeave, leaveId).observe(requireActivity()) {
                it.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let {
                                    Utils.showToast(
                                        requireActivity(),
                                        getString(R.string.txt_leave_sucess),
                                        Toast.LENGTH_LONG
                                    )

                                    editdata(resource.data.body())
                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(
                                requireContext(),
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {


                        }

                    }
                }
            }
        } else {
            leaveViewModel.addleave(addLeave).observe(requireActivity()) {
                it.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let {
                                    Utils.showToast(
                                        requireActivity(),
                                        getString(R.string.txt_leave_sucess),
                                        Toast.LENGTH_LONG
                                    )
                                    addData(leaveData = resource.data.body()?.data)
                                    //leaveList.clear()
                                    //getLeave(beforeTime, selectedMnt, selectYer, false)
                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(
                                requireContext(),
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {


                        }

                    }
                }
            }
        }
    }

    private fun editdata(editLeaveResponse: EditLeaveResponse?) {
        if(editLeaveResponse!=null) {
            for ((index, value) in leaveList.withIndex()) {
                if (value._id.equals(editLeaveResponse.data._id)) {
                    leaveList[index]=editLeaveResponse.data
                    leaveAdapter?.notifyItemChanged(index,editLeaveResponse.data)
                }
            }
        }

    }
    private fun addData(leaveData: LeaveList?) {
        if(leaveData!=null) {
            leaveList.add(0,leaveData)
            leaveAdapter?.notifyItemInserted(0)
        }
    }
    override fun dialogDelete(leaveId: String?) {
        val fragment = BreakAlertDialog(
            object : BreakAlertDialog.ClickListener {
                override fun onDoneClicked() {
                    cancelLeave(leaveId)
                }

                override fun onCancelClicked() {


                }
            },
            "Are you sure you want to cancel(delete) your leave?",
            "Yes",
            "Cancel"
        )

        fragment.show(requireActivity().supportFragmentManager, "alert")
        fragment.isCancelable = false
    }

    fun cancelLeave(leaveId: String?) {
        ApiClient.setToken()
        leaveViewModel.cancelLeave().observe(requireActivity()) {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {

                        if (resource.data?.isSuccessful!!) {
                            resource.data.let {

                             delete(leaveId,resource.data.body()?.code)
                            }
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {

                        Utils.showToast(
                            requireContext(), it.message.toString(), Toast.LENGTH_LONG
                        )
                    }
                    Status.LOADING -> {


                    }

                }
            }
        }
    }
    fun delete(leaveId: String?,code: String?) {
        ApiClient.setToken()
        var leaveDelete=LeaveDelete()
        leaveDelete.authentication_code=code
        leaveId?.let {
            leaveViewModel.deleteLeave(leaveDelete, it).observe(requireActivity()) {
                it.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {

                            if (resource.data?.isSuccessful!!) {
                                resource.data.let {
                                deleteData(leaveId)

                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {

                            Utils.showToast(
                                requireContext(), it.message.toString(), Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {


                        }

                    }
                }
            }
        }

    }
    fun deleteData(leaveId: String?){

        //getLeave(beforeTime, selectedMnt, selectYer, true)
        //Delete the Item from List
        for((index,value) in leaveList.withIndex()) {
            if(value._id.equals(leaveId)) {
                leaveList.removeAt(index)
                leaveAdapter?.notifyItemRemoved(index)

            }
        }

    }
    private fun setCurrentDay(){
        var calender=Calendar.getInstance()
        var formate= SimpleDateFormat("MMMM - yyyy")
        tvDate.text = formate.format(calender.time)
    }
}
