package com.stylist.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.stylist.R
import com.stylist.activities.AddPortfolioActivity
import com.stylist.activities.ImageDetailScreenActivity
import com.stylist.adapter.PortFolioAdapter
import com.stylist.custom.other.EqualSpacingItemDecoration
import com.stylist.data.PortFolioObj
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.PortFolioViewModel
import kotlinx.android.synthetic.main.fragment_portfolio_list.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.no_protfolio.*
import spencerstudios.com.bungeelib.Bungee


/**
 * Created by Hitesh Patel on 28,January,2021
 */
class PortfolioFragment: Fragment(),PortFolioAdapter.PortFolioListInterface {

    lateinit var portFolioListFrgViewModel: PortFolioViewModel
    var before: String? = null
    var after:String? = null
    var isLoading = false
    var porstList = ArrayList<PortFolioObj>()
    var portfolioAdapter: PortFolioAdapter? = null


    private fun setUpViewModel() {
        portFolioListFrgViewModel = ViewModelProvider(
            this, PortFolioViewModel.ViewModelFactory(
                ApiClient().apiService
            )
        ).get(PortFolioViewModel::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return LayoutInflater.from(context).inflate(
            R.layout.fragment_portfolio_list,
            container,
            false
        )
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startAnim()
        setUpViewModel()

        val parentFragment = parentFragment
        (parentFragment as ProfileFragment).fab.setOnClickListener {

            val intent = Intent(context, AddPortfolioActivity::class.java)
            startActivity(intent)
            Bungee.slideLeft(context)

        }

        portfolioAdapter = PortFolioAdapter(requireContext(), porstList)
        portfloList.addItemDecoration(
            EqualSpacingItemDecoration(
                12,
                EqualSpacingItemDecoration.GRID
            )
        )
        portfolioAdapter?.setListener(this@PortfolioFragment)
        portfloList?.adapter = portfolioAdapter




        portfloList.isNestedScrollingEnabled = false;
        nScroll.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (oldScrollY > 0) {
                val visibleItemCount = portfloList?.layoutManager?.childCount
                val totalItemCount = portfloList?.layoutManager?.itemCount
                val pastVisiblesItems =
                    (portfloList?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()



                if (isLoading) {


                    if (visibleItemCount != null) {
                        if (visibleItemCount.plus(pastVisiblesItems) >= totalItemCount ?: 0) {


                            isLoading = false
                            if (portfolioAdapter != null) {


                                progressBar?.visibility = View.VISIBLE


                                val handler = Handler()
                                handler.postDelayed({

                                    isLoading = true

                                    try {
                                        before =
                                            porstList[porstList.size - 1].created_at
                                    } catch (e: Exception) {

                                    }

                                    getPortFolioData(before, null)

                                }, 2000)
                            }
                        }
                    }
                }
            }
        })
    }


    private fun startAnim() {
        if (avLoadingCategory != null)
            avLoadingCategory.show()

    }

    private fun stopAnim() {
        if (avLoadingCategory != null)
            avLoadingCategory.hide()

    }

    override fun onResume() {
        super.onResume()
        before = null
        after = null
        getPortFolioData(before, after)
    }


    private fun getPortFolioData(before: String?, after: String?){
        ApiClient.setToken()
        portFolioListFrgViewModel.getPortfolio(before, after)
            .observe(requireActivity()) {
                it?.let { resource ->
                    when (resource.status) {
                        Status.SUCESS -> {
                            if (resource.data?.isSuccessful!!) {
                                resource.data?.let { response ->
                                    setResposerData(
                                        response.body()?.portfolio
                                    )
                                }
                            } else {
                                Utils.setErrorData(requireContext(), resource.data.errorBody())
                            }

                        }
                        Status.ERROR -> {
                            Utils.showToast(
                                requireContext(),
                                it.message.toString(),
                                Toast.LENGTH_LONG
                            )
                        }
                        Status.LOADING -> {

                        }

                    }
                }
            }
    }


    override fun onListRowClick(portflowObj: PortFolioObj, position: Int) {
        val intent = Intent(context, ImageDetailScreenActivity::class.java)
        intent.putExtra("imageUrl", portflowObj.url)
        intent.putExtra("position", position)
        intent.putExtra("obj", portflowObj)
        startActivityForResult(intent, 240)
        Bungee.slideLeft(context)
    }


    private fun setResposerData(response: ArrayList<PortFolioObj>?) {

        try {


            if (portfolioAdapter == null && response != null) {
                before = null
                after = null
                porstList.clear()
                porstList.addAll(response)
                portfloList?.adapter = portfolioAdapter
                portfolioAdapter?.notifyDataSetChanged()


            } else if (porstList != null && response != null) {

                if (before != null && after == null) {

                    porstList.addAll(response)

                    try {
                        portfolioAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    progressBar?.visibility = View.GONE

                } else if (before == null && before != null) {

                    response.forEach {
                        if (porstList.contains(it)) {
                            val index = porstList.indexOf(it)
                            porstList.removeAt(index)
                            porstList.add(0, it)
                        } else {
                            porstList.add(0, it)
                        }
                    }

                    try {
                        portfolioAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    progressBar?.visibility = View.GONE

                } else {


                    porstList.clear()
                    response.forEach {
                        if (porstList.contains(it)) {
                            val index = porstList.indexOf(it)
                            porstList.removeAt(index)
                            porstList.add(it)
                        } else {
                            porstList.add(it)
                        }

                    }
                    try {
                        portfolioAdapter?.notifyDataSetChanged()
                    } catch (e: Exception) {

                    }
                    progressBar?.visibility = View.GONE
                }

            }


            isLoading = true


            progressBar?.visibility = View.GONE

            emptyView()

        } catch (e: Exception) {
            isLoading = true


            progressBar?.visibility = View.GONE

            emptyView()

        }
    }

    private fun emptyView() {
        if (porstList.isNotEmpty()) {
            stopAnim()
            constrainEmptyOrder.visibility = View.GONE
            portfloList.visibility = View.VISIBLE


        } else {
            stopAnim()

            portfloList.visibility = View.GONE
            constrainEmptyOrder.visibility = View.VISIBLE

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 240) {
            if (resultCode == Activity.RESULT_OK) {

                val position = data?.getIntExtra("position", -1)
                val obj:PortFolioObj? = data?.getParcelableExtra<PortFolioObj>("objPort")
                if (position != null && position > -1) {
                    porstList?.removeAt(position)
                    portfolioAdapter?.notifyDataSetChanged()
                    emptyView()
                }
            }
        }
    }
}