package com.stylist.fragments

import android.app.ActionBar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.applandeo.materialcalendarview.EventDay
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener
import com.applandeo.materialcalendarview.listeners.OnDayClickListener
import com.stylist.R
import com.stylist.adapter.Appointmentdapter
import com.stylist.data.AppointmentDetails
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.AppointmentViewModel
import kotlinx.android.synthetic.main.fragment_appointment_copy.*
import kotlinx.android.synthetic.main.include_appointment.*
import kotlinx.android.synthetic.main.no_appointment.*
import java.text.SimpleDateFormat
import java.util.*


class AppointmentFragment : Fragment(), View.OnClickListener {

    val events: MutableList<EventDay> = ArrayList()
    lateinit var imgDown:ImageView
    private lateinit var appointmentViewModel: AppointmentViewModel
    var mCalendar: Calendar? = null
    private lateinit var rlSubTIttle:RelativeLayout
    private lateinit var  tvSubTitile :TextView
    private lateinit var appointmentdapter: Appointmentdapter
    var isExpand:Boolean=false
    var isDayClick:Boolean=false
    var startDate:String?=null
    var endDate:String?=null
    private val mDateFormat = SimpleDateFormat("dd MMMM, yyyy", /*Locale.getDefault()*/ Locale.ENGLISH)
     private val dateFormate = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_appointment_copy, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViewModel()



        mCalendar = Calendar.getInstance()
        mCalendar!!.set(
            mCalendar!!.get(Calendar.YEAR), mCalendar!!.get(Calendar.MONTH), mCalendar!!.get(
                Calendar.DATE
            ), 0, 0, 0
        )

        startDate=startDate(mCalendar!!)
        endDate=endDate(mCalendar!!)


        tvSubTitile= requireActivity().findViewById<View>(R.id.tvSubTitle) as TextView
        rlSubTIttle= requireActivity().findViewById<View>(R.id.rlSubTIttle) as RelativeLayout
        imgDown= requireActivity().findViewById<View>(R.id.imgDown) as ImageView

        tvSubTitile.text = mDateFormat.format(Date())
        imgDown.visibility=View.VISIBLE
        rlSubTIttle.setOnClickListener(this)


        calendarView.setOnDayClickListener(object : OnDayClickListener {
            override fun onDayClick(eventDay: EventDay) {
                isDayClick=true

                setCollpse()

               tvSubTitile.text = mDateFormat.format(eventDay.calendar.time)

                val startTime =setStartTime(calender = eventDay.calendar).time
                startDate= dateFormate.format(startTime)


                val endTime =setEndTime(eventDay.calendar).time
                endDate= dateFormate.format(endTime)

                getAppointment()


            }
        })
        calendarView.setOnForwardPageChangeListener(object : OnCalendarPageChangeListener {
            override fun onChange() {
                tvSubTitile.text = mDateFormat.format(calendarView.currentPageDate.time)
                startDate = startDate(calendarView.currentPageDate)
                endDate = endDate(calendarView.currentPageDate)
                getAppointment()


            }
        })

        calendarView.setOnPreviousPageChangeListener(object : OnCalendarPageChangeListener {
            override fun onChange() {
                tvSubTitile.text = mDateFormat.format(calendarView.currentPageDate.time)
                startDate = startDate(calendarView.currentPageDate)
                endDate = endDate(calendarView.currentPageDate)
                getAppointment()
            }
        })



    }

    override fun onResume() {
        super.onResume()
        getAppointment()
    }
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.rlSubTIttle -> {
                setCollpse()
            }

        }
    }
    private fun setCollpse(){

        if (isExpand) {
            isExpand = false
            expand(appbarLayout)
        } else {
            isExpand = true
            collapse(appbarLayout)
        }
    }


    private fun setupViewModel() {
        appointmentViewModel =
            ViewModelProvider(
                this,
                AppointmentViewModel.ViewModelFactory(ApiClient().apiService)
            )
                .get(AppointmentViewModel::class.java)
    }


    private fun getAppointment() {
        ApiClient.setToken()
        appointmentViewModel.getAppointment(startDate, endDate).observe(requireActivity()) {
            it.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                           stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data.let { it1 ->
                                resource.data.body()?.data?.let { it2 -> setUpReviewUI(it2) }
                            }
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                    startAnim()

                    }

                }
            }
        }
    }
    private fun setUpReviewUI(appointment: ArrayList<AppointmentDetails>) {

        if(!isDayClick){
            isDayClick=false
            setEvents(appointment)
        }

        if (appointment != null && appointment.size > 0) {

            tvReview.visibility = View.VISIBLE
            rcAppointment.visibility = View.VISIBLE
            appointmentdapter = Appointmentdapter(requireContext(), appointment)
            rcAppointment.adapter = appointmentdapter
            setEvents(appointment)
            constrainEmptyOrder.visibility = View.GONE

        } else {
            constrainEmptyOrder.visibility = View.VISIBLE
            rcAppointment.visibility = View.GONE
            tvReview.visibility = View.GONE
        }


    }

    fun setEvents(appointment: ArrayList<AppointmentDetails>) {


        for (days in appointment.withIndex()) {
            var date= days.value.start_time?.let { Utils.getTimes(it) }
            val calendar1 = Calendar.getInstance()
            calendar1.timeInMillis = date!!
            calendar1.add(Calendar.DAY_OF_MONTH, 0)
            events.add(EventDay(calendar1, R.drawable.round_badge))
        }

        calendarView.setEvents(events)

    }
    fun endDate(calender: Calendar): String{


        //gives 30 date of any month
        calender.add(Calendar.MONTH, 0);
        calender[Calendar.DATE] = calender.getActualMaximum(Calendar.DAY_OF_MONTH)

        val monthLastDay =setEndTime(calender).time
        return dateFormate.format(monthLastDay)
    }
    fun startDate(calender: Calendar): String {


        //gives 1 date of any month
        calender.add(Calendar.MONTH, 0)
        calender[Calendar.DATE] = calender.getActualMinimum(Calendar.DAY_OF_MONTH)

        val monthFirstDay = setStartTime(calender).time
        return dateFormate.format(monthFirstDay)

    }
    fun expand(v: View) {

        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = v.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.layoutParams.height = 1
        v.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) ActionBar.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        if(::imgDown.isInitialized)
            imgDown.animate().rotation(180f)
        // Expansion speed of 1dp/ms
        a.setDuration((targetHeight / v.context.resources.displayMetrics.density).toLong())
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        if(::imgDown.isInitialized)
            imgDown.animate().rotation(0f)
        // Collapse speed of 1dp/ms
        a.setDuration((initialHeight / v.context.resources.displayMetrics.density).toLong())
        v.startAnimation(a)
    }
    fun setStartTime(calender: Calendar):Calendar{
        calender.set(Calendar.HOUR, 0)
        calender.set(Calendar.MINUTE, 0)
        calender.set(Calendar.SECOND, 0)
        calender.set(Calendar.MILLISECOND, 0)
        return calender

    }
    fun setEndTime(calender: Calendar):Calendar{

        calender.set(Calendar.HOUR, 23)
        calender.set(Calendar.MINUTE, 59)
        calender.set(Calendar.SECOND, 59)
        calender.set(Calendar.MILLISECOND, 0)
        return calender
    }
    private fun startAnim() {
        if (avLoading != null)
            avLoading.show()
    }

    private fun stopAnim() {
        if (avLoading != null)
            avLoading.hide()


    }
}