package com.stylist.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.stylist.R
import com.stylist.utils.SharedPrefrence
import com.stylist.utils.Utils
import kotlinx.android.synthetic.main.fragment_aboutus.*

class AboutUsFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_aboutus, container, false)
    }



    override fun onResume() {
        super.onResume()
        setUpUi()
    }


    private fun setUpUi() {

        val info = SharedPrefrence.getUserInfo(requireContext())
        val birthdate = SharedPrefrence.geBirthday(requireContext())
        val gender = SharedPrefrence.getGender(requireContext())
        val mobile_number = SharedPrefrence.getMobileNo(requireContext())
        val email = SharedPrefrence.getuserEmail(requireContext())
        val BcontactNumber = SharedPrefrence.getBranchContact(requireContext())
        val BAbout = SharedPrefrence.getBranchInfo(requireContext())
        val saloonType = SharedPrefrence.getSalooonType(requireContext())
        val Branchemail = SharedPrefrence.getBranchEmail(requireContext())
        val BranchName = SharedPrefrence.getBrnachName(requireContext())

        if (!info.isNullOrEmpty()) {
            tvTitleDescription?.visibility = View.VISIBLE
            tvTitleDescription.text = info

        } else {
            tvTitleDescription.visibility = View.GONE
        }

        if (!birthdate.isNullOrEmpty())
            tvValue.text = Utils.getConvertBirthDate(birthdate.toString())
        else {
            tvValue.text = "-"
        }

        if (!gender.isNullOrEmpty()) {
            tvGenderValue.text = gender.substring(0, 1).toUpperCase() + gender.substring(1).toLowerCase();

        }
        else {
            tvGenderValue.text = "-"
        }

        if (!mobile_number.isNullOrEmpty()) {
            tvMobileValue.text = mobile_number

        }else{
            tvMobileValue.text = "-"
        }

        if (!email.isNullOrEmpty())
            tvEmailValue.text = email
        else{
            tvEmailValue.text = "-"
        }


        if(!saloonType.isNullOrEmpty()) {
            //tvSaloonType.text = saloonType
            var gndtxt = "Male"
            if (saloonType.equals("male")) {
                gndtxt = "Male"
            } else if (saloonType.equals("female")) {
                gndtxt = "Female"
            } else {
                gndtxt = "Unisex"
            }
            tvSaloonType.text = gndtxt

        }else
        tvSaloonType.text = "-"

        if (!BcontactNumber.isNullOrEmpty())
            tvPhoneType.text = BcontactNumber


        if(!Branchemail.isNullOrEmpty())
            tvEmail2Value.text = Branchemail
        else
            tvEmail2Value.text = "-"

        if(!BranchName.isNullOrEmpty())
            tvSaloonNameVal.text = BranchName
        else
            tvSaloonNameVal.text = "-"


        if (!BAbout.isNullOrEmpty())
            tvSaloonDesciption.text = BAbout
        else{
            tvSaloonDesciption.visibility=View.GONE
        }





        tvPhoneType?.setOnClickListener {
            val callIntent = Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode(BcontactNumber?:"")))
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            requireContext().startActivity(callIntent)
        }

    }


}