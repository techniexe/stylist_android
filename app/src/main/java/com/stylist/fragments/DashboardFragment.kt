package com.stylist.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.stylist.R
import com.stylist.adapter.Reviewdapter
import com.stylist.data.DashBoard
import com.stylist.data.FeedBackDetail
import com.stylist.services.ApiClient
import com.stylist.services.Status
import com.stylist.utils.Utils
import com.stylist.viewmodels.DashboardViewModel
import com.stylist.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.include_bottom_dashboard.*
import kotlinx.android.synthetic.main.include_bottom_right_dashboard.*
import kotlinx.android.synthetic.main.include_review.*
import kotlinx.android.synthetic.main.include_top_dashboard.*
import kotlinx.android.synthetic.main.include_top_right_dashboard.*
import kotlinx.android.synthetic.main.no_review.*


class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel

    private lateinit var userViewModel: UserViewModel
    private lateinit var reviewdapter: Reviewdapter
    var beforeTime: String? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onResume() {
        super.onResume()
        setupViewModel()
        getDashBoardData()
    }
    private fun setupViewModel() {
        dashboardViewModel =
            ViewModelProvider(this, DashboardViewModel.ViewModelFactory(ApiClient().apiService))
                .get(DashboardViewModel::class.java)

        userViewModel =
            ViewModelProvider(this, UserViewModel.ViewModelFactory(ApiClient().apiService))
                .get(UserViewModel::class.java)


    }
    private fun getDashBoardData(){
        ApiClient.setToken()
        dashboardViewModel.getDashBoardData().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                        stopAnim()
                        getFeedBack()

                        if (resource.data?.isSuccessful!!) {
                            setupUI(resource.data.body()?.data)
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }
                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()
                    }
                }
            }
        })
    }



    private fun startAnim() {
        if (avLoading != null)
            avLoading.show()
    }

    private fun stopAnim() {
        if (avLoading != null)
            avLoading.hide()


    }
    private fun setupUI(dashBoardData: DashBoard?) {
        if (dashBoardData != null) {
            tvTotalAppointment.text=Utils.setPrecesionFormate(dashBoardData.totalAppointment)
            tvCompleteAppointment.text=Utils.setPrecesionFormate(dashBoardData.completedAppointment)
            tvCancelAppointment.text=Utils.setPrecesionFormate(dashBoardData.cancelledAppointment)
            tvLeaveComplete.text=Utils.setPrecesionFormate(dashBoardData.totalLeaves)
        }

    }
    private fun getFeedBack(){
        ApiClient.setToken()
        userViewModel.getFeedback(beforeTime).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCESS -> {
                        stopAnim()
                        if (resource.data?.isSuccessful!!) {
                            resource.data?.let { it1 -> setUpReviewUI(it1.body()?.data) }
                        } else {
                            Utils.setErrorData(requireContext(), resource.data.errorBody())
                        }

                    }
                    Status.ERROR -> {
                        stopAnim()
                        Utils.showToast(requireContext(), it.message.toString(), Toast.LENGTH_LONG)
                    }
                    Status.LOADING -> {
                        startAnim()

                    }

                }
            }
        })
    }
    private fun setUpReviewUI(reviewList: ArrayList<FeedBackDetail>?) {

        if (reviewList != null && reviewList.size > 0) {
            tvReview.visibility = View.VISIBLE
            rvReviews.visibility = View.VISIBLE
            reviewdapter = Reviewdapter(requireContext(), reviewList)
            rvReviews.adapter = reviewdapter
            constrainEmptyOrder.visibility = View.GONE
        } else {
            constrainEmptyOrder.visibility = View.VISIBLE
            rvReviews.visibility = View.GONE
            tvReview.visibility = View.GONE
        }

    }
}