package com.stylist.repositary

import com.stylist.services.ApiInterface


class UserRepositary(private val apiInterface: ApiInterface){

    suspend fun getFeedBack(before: String?)=apiInterface.getFeedback(before)
    suspend fun getUserProfile()=apiInterface.getProfile()
}