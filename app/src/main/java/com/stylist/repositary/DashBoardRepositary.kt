package com.stylist.repositary

import com.stylist.services.ApiInterface


class DashBoardRepositary(private val apiInterface: ApiInterface){

    suspend fun getDashBoardData()=apiInterface.getDashBoardData()
}