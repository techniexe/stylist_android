package com.stylist.repositary

import com.stylist.data.EditUserPostObj
import com.stylist.services.ApiInterface
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 17,February,2021
 */
class EditUserDetailRepository(private val apiInterface: ApiInterface) {

    suspend fun uploadUserDetail(editUserPostObj: EditUserPostObj) = apiInterface.patchUserData(editUserPostObj)


    suspend fun uploadUserImageServer(parts:MultipartBody.Part) = apiInterface.uploadUserPic(parts)
}