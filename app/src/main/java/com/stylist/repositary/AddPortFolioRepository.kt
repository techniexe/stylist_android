package com.stylist.repositary

import com.stylist.services.ApiInterface
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 16,February,2021
 */
class AddPortFolioRepository (val apiInterface: ApiInterface){

    suspend fun uploadPortfolioServer(parts:ArrayList<MultipartBody.Part>) = apiInterface.uploadPortFolioToServer(parts)
}