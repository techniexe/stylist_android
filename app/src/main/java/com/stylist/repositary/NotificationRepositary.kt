package com.stylist.repositary

import com.stylist.services.ApiInterface


/**
 * Created by Hitesh Patel on 31,August,2021
 */
class NotificationRepositary(val apiInterface: ApiInterface) {
    suspend fun notificationList( before: String?,after:String?) = apiInterface.getNotifications(before,after)
    suspend fun notificationSeen(notificationId: String?) = apiInterface.notificationItemSeen(notificationId)
    suspend fun notificationCount() = apiInterface.getNotificationCount()
}