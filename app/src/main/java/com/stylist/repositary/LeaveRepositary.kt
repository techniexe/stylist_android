package com.stylist.repositary

import com.stylist.data.AddLeave
import com.stylist.data.Code
import com.stylist.data.LeaveDelete
import com.stylist.services.ApiInterface


class LeaveRepositary(private val apiInterface: ApiInterface){

    suspend fun getLeaveData(month:String?,  year:String?,  before:String?, after:String?)=apiInterface.getLeaveData(month,year,before,after)
    suspend fun addLeave(addLeave: AddLeave)=apiInterface.addLeave(addLeave)
    suspend fun editLeave(addLeave: AddLeave,leaveId:String)=apiInterface.editLeave(addLeave,leaveId)
    suspend fun cancelLeave()=apiInterface.leaveCancel()
    suspend fun deleteLeave(code: LeaveDelete, leaveId: String)=apiInterface.deleteLeave(code,leaveId)
}