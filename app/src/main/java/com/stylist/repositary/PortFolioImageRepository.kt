package com.stylist.repositary

import com.stylist.data.PortFolioMediaObj
import com.stylist.services.ApiInterface


/**
 * Created by Hitesh Patel on 12,February,2021
 */
class PortFolioImageRepository (val apiInterface:ApiInterface){

    suspend fun deletePortFolioImageData(idsObj: PortFolioMediaObj) = apiInterface.deletePortfolioMedia(idsObj)
}