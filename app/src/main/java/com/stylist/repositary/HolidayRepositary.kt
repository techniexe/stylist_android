package com.stylist.repositary

import com.stylist.services.ApiInterface
import retrofit2.http.Query


/**
 * Created by Hitesh Patel on 08,February,2021
 */
class HolidayRepositary(private val apiInterface: ApiInterface) {

    suspend fun getHolidayList( month:String?,  year:String?,  before:String?, after:String?, holiday_name:String?) = apiInterface.getHolidayList(month,year,before,after,holiday_name)
}