package com.stylist.repositary

import com.stylist.services.ApiInterface


class AppointmentRepositary(private val apiInterface: ApiInterface){

    suspend fun getAppointment(startDate: String?, EndDate: String?) =apiInterface.getAppoinment(startDate,EndDate)
}