package com.stylist.repositary

import com.stylist.data.PortFolioMediaObj
import com.stylist.services.ApiInterface


/**
 * Created by Hitesh Patel on 28,January,2021
 */

class PortFolioRepositary (val apiInterface: ApiInterface){
    suspend fun getPortfolioData(before: String?,after:String?) = apiInterface.getPortFolioRes(before,after)


}