package com.stylist.repositary

import com.stylist.data.SessionRequest
import com.stylist.services.ApiInterface

class SessionRepositary(private val apiInterface: ApiInterface){

    suspend fun getSessionToken(sessionRequest: SessionRequest) = apiInterface.getSessionToken(sessionRequest)
}