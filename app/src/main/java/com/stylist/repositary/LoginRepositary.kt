package com.stylist.repositary

import com.stylist.services.ApiInterface


class LoginRepositary(private val apiInterface: ApiInterface){

    suspend fun verifyOtp(mobNumer:String,code:String)=apiInterface.verifyOtp(mobileNumber = mobNumer,code = code)

    suspend fun getOtpByMobNo(mobNo:String,method:String)=apiInterface.getOtpByMobNum(mobNo,method)
}