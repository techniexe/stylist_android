package com.stylist.dialog

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.text.InputFilter
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.stylist.R
import com.stylist.data.AddLeave
import com.stylist.data.LeaveList
import com.stylist.services.ApiClient
import com.stylist.utils.Utils
import com.stylist.viewmodels.LeaveViewModel
import kotlinx.android.synthetic.main.add_leave_activity.*
import kotlinx.android.synthetic.main.add_leave_activity.view.*
import kotlinx.android.synthetic.main.custom_toast.view.*
import spencerstudios.com.bungeelib.Bungee
import java.util.*


class AddLeaveDialog(var leaveList: LeaveList?,var adapterPosition: Int) : DialogFragment() {

    private var mListner: DialogToFragment? = null
    var currentDate: Long = System.currentTimeMillis() - 1000
    var fromDate: Long = 0
    var toDate: Long = 0

lateinit var fromEditText: EditText
    lateinit var etEditLeave: EditText
    lateinit var toEditText: EditText
    var validFromDate = false
    var validToDate = false
    private lateinit var leaveViewModel: LeaveViewModel

    interface DialogToFragment {
        fun dialogDismiss()
        fun dialogSaveAddEdit(addLeave: AddLeave, leaveId: String?, adapterPosition: Int)
        fun dialogDelete(leaveId:String?)
    }

    fun setListener(mListener: DialogToFragment) {
        this.mListner = mListener
    }
    override fun onResume() {
        super.onResume()
        val window = dialog!!.window
        window?.setGravity(Gravity.CENTER)


    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.window?.setBackgroundDrawable(null)
        dialog!!.setCanceledOnTouchOutside(true)
        dialog!!.window?.attributes?.windowAnimations = R.style.DialogAnimation
        return super.onCreateView(inflater, container, savedInstanceState)
    }

 /*   override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
        setupViewModel()

    }

    private fun setupViewModel() {
        leaveViewModel =
            ViewModelProvider(this, LeaveViewModel.ViewModelFactory(ApiClient().apiService))
                .get(LeaveViewModel::class.java)
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)
        val inflater = requireActivity().layoutInflater

        val view = inflater.inflate(R.layout.add_leave_activity, null)
        builder.setView(view)
        fromEditText=view.fromText
        toEditText=view.toText
        etEditLeave=view.etLeave
        if(leaveList!=null)
        {
            if(!leaveList?.reason.isNullOrEmpty()){
                etEditLeave.setText(leaveList!!.reason)
            }

            if(!leaveList?.start_time.isNullOrEmpty()){
                var date= leaveList?.start_time?.let { Utils.getConvertDate(it) }
                fromEditText.setText(date)
                // currentDate= leaveList?.start_time?.let { Utils.getConvertDateToLong(it) }!!
            }

            if(!leaveList?.end_time.isNullOrEmpty()){
                var date= leaveList?.end_time?.let { Utils.getConvertDate(it) }
                toEditText.setText(date)
            }

            fromDate = leaveList?.start_time?.let { Utils.getConvertDateToLong(it) }!!
            validFromDate = true

            toDate = leaveList?.end_time?.let { Utils.getConvertDateToLong(it) }!!
            validToDate = true

        }
        fromEditText.setOnClickListener {
            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(
                requireActivity(),
                { view, year, month, dayOfMonth ->
                    fromEditText.setText(getNumString(dayOfMonth) + " " + getNumMonth(month + 1) + " " + year.toString())
                    fromDate = Utils.getDateTime(year, month, dayOfMonth)
                    validFromDate = true
                },
                newDate.get(Calendar.YEAR),
                newDate.get(Calendar.MONTH),
                newDate.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.datePicker.minDate = currentDate
            datePicker.show()
        }

        toEditText.setOnClickListener {
            val newDate = Calendar.getInstance()
            val datePicker = DatePickerDialog(
                requireActivity(),
                { view, year, month, dayOfMonth ->
                    toEditText.setText(getNumString(dayOfMonth) + " " + getNumMonth(month + 1) + " " + year.toString())
                    toDate = Utils.getDateTime(year, month, dayOfMonth)
                    validToDate = true
                },
                newDate.get(Calendar.YEAR),
                newDate.get(Calendar.MONTH),
                newDate.get(Calendar.DAY_OF_MONTH)
            )
            if(fromDate!=0L)
            datePicker.datePicker.minDate = fromDate
            else
            datePicker.datePicker.minDate = currentDate

            datePicker.show()
        }
        view.imgClose.setOnClickListener {
            Bungee.slideDown(requireActivity())
            dismiss()

        }
        view.btnSignIn.setOnClickListener {
            val addLeave = AddLeave()
            var fDate=""
            var tDate=""

            if (validateFileds()) {
                if (fromDate != 0L) {
                    fDate = Utils.getIsoDate(fromDate)

                }
                if (toDate != 0L) {
                    tDate = Utils.getIsoDate(toDate)
                }
                addLeave.reason = etEditLeave.text.trim().toString()
                addLeave.start_time = fDate
                addLeave.end_time = tDate
                if(leaveList!=null)
                mListner!!.dialogSaveAddEdit(addLeave,leaveList?._id!!,adapterPosition)
                else
                mListner!!.dialogSaveAddEdit(addLeave, null, 0)
                dismiss()
            } else {


            }
        }

        return builder.create()
    }



    fun getNumString(num: Int): String {
        if (num < 10) {
            return "0" + num
        }
        return num.toString()
    }

    fun getNumMonth(num: Int): String {
        when (num) {
            1 -> {
                return "Jan"
            }
            2 -> {
                return "Feb"
            }
            3 -> {
                return "Mar"
            }
            4 -> {
                return "Apr"
            }
            5 -> {
                return "May"
            }
            6 -> {
                return "Jun"
            }
            7 -> {
                return "Jul"
            }
            8 -> {
                return "Aug"
            }
            9 -> {
                return "Sep"
            }
            10 -> {
                return "Oct"
            }
            11 -> {
                return "Nov"
            }
            12 -> {
                return "Dec"
            }

        }

        return num.toString()
    }

    fun validateFileds(): Boolean {

        if (etEditLeave.text.isEmpty()) {
            Utils.showToast(
                requireActivity(),
                getString(R.string.txt_complaindescription),
                Toast.LENGTH_LONG
            )
            return false
        }
        if (etEditLeave.text.length < 10) {

            etEditLeave.error = "Minimum 10 characters required"
            return false
        }
        if (fromDate == 0L) {
            Utils.showToast(requireActivity(), "Invalid Start Date", Toast.LENGTH_SHORT)
            return false
        }

        if (toDate == 0L) {

            Utils.showToast(requireActivity(), "Invalid End Date", Toast.LENGTH_SHORT)
            return false
        }

        if (fromDate != 0L) {
            if (toDate == 0L) {
                Utils.showToast(requireActivity(), "Invalid End Date", Toast.LENGTH_SHORT)
                return false
            }
        }
        if (toDate != 0L) {
            if (fromDate == 0L) {
                Utils.showToast(requireActivity(), "Invalid Start date", Toast.LENGTH_SHORT)
                return false
            }
        }


        if (fromDate.compareTo(toDate) == 0) {
            //Date1 is equal Date2
            return true
        } else if (fromDate.compareTo(toDate) > 0) {
            //Date1 is after Date
            Utils.showToast(
                requireActivity(), "Start date can't be after  End date", Toast
                    .LENGTH_SHORT
            )
            return false

        } else if (fromDate.compareTo(toDate) < 0) {
            //Date1 is before Datei
            return true

        }

        return true

    }

    // ignore enter First space on edittext
    private fun ignoreFirstWhiteSpace(): InputFilter {
        return InputFilter { source, start, end, dest, dstart, dend ->
            for (i in start until end) {
                if (Character.isWhitespace(source[i])) {
                    if (dstart == 0) return@InputFilter ""
                }
            }
            null
        }
    }

}
