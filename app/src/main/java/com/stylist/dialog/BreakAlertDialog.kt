package com.stylist.dialog


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.stylist.R


/**
 * Created by Hitesh Patel on 12,February,2021
 */
class BreakAlertDialog : DialogFragment {
    var mListener: ClickListener? = null
    private var mTitle: String? = null
    private val mMsg: String? = null
    private var mPostiveText: String? = null
    private var mNegativeText: String? = null
    private val mUserImage: String? = null

    @SuppressLint("ValidFragment")
    constructor() {
    }

    @SuppressLint("ValidFragment")
    constructor(
        dialogListener: ClickListener?,
        title: String?,
        positiveBtn: String?,
        negativeBtn: String?
    ) {
        mListener = dialogListener
        mTitle = title
        mPostiveText = positiveBtn
        mNegativeText = negativeBtn
    }

    fun setDialogListener(dialogListener: ClickListener?) {
        mListener = dialogListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val v: View = inflater.inflate(R.layout.break_dialog, container, false)
        val title = v.findViewById<View>(R.id.titleAlertDialog) as TextView
        val doneButton = v.findViewById<View>(R.id.postiveBtnAlertDialog) as Button
        val cancelButton = v.findViewById<View>(R.id.neagtiveBtnAlertDialog) as Button
        doneButton.setOnClickListener {
            mListener!!.onDoneClicked()
            dismiss()
        }
        cancelButton.setOnClickListener {
            mListener!!.onCancelClicked()
            dismiss()
        }
        title.text = mTitle
        doneButton.text = mPostiveText
        cancelButton.text = mNegativeText
        if (mNegativeText == null || mNegativeText!!.isEmpty()) {
            cancelButton.visibility = View.GONE
        }
        dialog!!.window!!.setBackgroundDrawableResource(R.color.colortransparent)
        return v
    }

    interface ClickListener {
        fun onDoneClicked()
        fun onCancelClicked()
    }
}