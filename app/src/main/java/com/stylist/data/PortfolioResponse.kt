package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import okhttp3.MultipartBody


/**
 * Created by Hitesh Patel on 28,January,2021
 */
@Parcelize
data class PortfolioResponse(val portfolio: ArrayList<PortFolioObj>) : Parcelable


@Parcelize
data class PortFolioObj(
    var _id: String, var url: String?, var thumbnail_url: String?, var thumbnail_low_url: String?,
    var type: String?, var sequence: Int, var created_at: String?
) : Parcelable


@Parcelize
data class PortFolioMediaObj(var portfolioIds:ArrayList<String>):Parcelable

