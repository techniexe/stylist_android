package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SessionRequest(val accessToken: String, val registrationToken: String) : Parcelable


