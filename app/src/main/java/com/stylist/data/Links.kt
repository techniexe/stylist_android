package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Links (
    val website: String? = null,
    val instagram: String? = null,
    val twitter: String? = null,
    val youtube: String? = null,
    val facebook: String? = null,
    val medium: String? = null,
    val linkedin: String? = null
) : Parcelable

