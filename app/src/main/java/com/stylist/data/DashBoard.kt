package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DashBoard(
    val totalAppointment: Double? = null,
    val completedAppointment: Double? = null,
    val cancelledAppointment: Double? = null,
    val totalLeaves: Double? = null
):Parcelable



