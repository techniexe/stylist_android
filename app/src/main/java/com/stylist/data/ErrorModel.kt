package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class ErrorRes(val error: ErrorModel) : Parcelable


@Parcelize
data class ErrorModel(val message: String, val email_exists: Boolean, val full_name: String, val facebook_id: String) : Parcelable