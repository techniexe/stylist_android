package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


/**
 * Created by Hitesh Patel on 08,February,2021
 */

@Parcelize
data class HolidayResponse(val data: ArrayList<HolidayResListData>) : Parcelable


@Parcelize
data class HolidayResListData(var _id:String,var holiday_name:String,var branch:HolidayBranchObj,var vendor_id:String,var date:String,var created_at:String,var updated_at:String):Parcelable

@Parcelize
data class HolidayBranchObj(var _id:String,var name:String,var media:ArrayList<HolidayBranchMedia>?):Parcelable

@Parcelize
data class HolidayBranchMedia(var sequence:Int?,var _id:String?,var url:String?,var thumbnail_url:String?,var thumbnail_low_url:String?,var type:String?):Parcelable