package com.techniexe.testing.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Order(
    var _id: String? = "",
    var display_id: String? = ""

) : Parcelable

