package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class LoginResponse(val customToken: String) : Parcelable