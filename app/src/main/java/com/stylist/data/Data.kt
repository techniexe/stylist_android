package com.stylist.data


data class Data<out T>(val data: T)