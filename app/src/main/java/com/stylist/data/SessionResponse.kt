package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SessionResponse(val token: String) : Parcelable