package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EditLeaveResponse(
    val branch_name: String,
    val data: LeaveList
):Parcelable