package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WorkingHoursMon (
    val start_time: String? = null,
    val end_time: String? = null,
    val open: Boolean? = null
) : Parcelable

