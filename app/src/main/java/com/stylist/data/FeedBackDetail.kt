package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FeedBackDetail (
        val _id: String? = null,
        val user_id: String? = null,
        var rating: Double? = null,
        var text: String? = null,
        var created_at: String? = null,
        var user_full_name: String? = null,
        val user_profile_pic: String? = null,
        val user_thumbnail_url: String? = null,
        val user_thumbnail_low_url: String? = null
) :Parcelable

@Parcelize
data class FeedbackData(var data: ArrayList<FeedBackDetail>) : Parcelable