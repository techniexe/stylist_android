package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import javax.sql.StatementEvent


/**
 * Created by Hitesh Patel on 17,February,2021
 */

@Parcelize
data class EditUserPostObj(var full_name:String?,var info:String?,var mobile_number:String?,var email:String?,var gender:String?,var birthdate:String?,var total_experience_year:Int?,var total_experience_month:String?,var skill:ArrayList<String>?):Parcelable