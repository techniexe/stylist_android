package com.stylist.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BranchData (
    val _id: String? = null,
    val location: Location? = null,
    val name: String? = null,
    val contact_number: String? = null,
    val contact_person_name: String? = null,
    val place_id: String? = null,
    val address_line1: String? = null,
    val city_id: String? = null,
    val state_id: String? = null,
    val pincode: String? = null,
    val complimentary: ArrayList<String>? = null,
    val links: Links? = null,
    val stylist_gender: String? = null,
    val serve_gender: String? = null,
    val about: String? = null,
    val city_name: String? = null,
    val state_name: String? = null,
    val country_id:Long? = null,
    val country_code: String? = null,
    val country_name: String? = null
): Parcelable

