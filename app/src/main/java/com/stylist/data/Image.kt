package com.stylist.data

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by Hitesh Patel on 15,February,2021
 */
class Image : Parcelable {
    private var id: Long
    private var name: String?
    private var path: String?
    private var countNum = 0

    constructor(id: Long, name: String?, path: String?) {
        this.id = id
        this.name = name
        this.path = path
    }

    protected constructor(p: Parcel) {
        id = p.readLong()
        name = p.readString()
        path = p.readString()
    }

  public  fun getId(): Long {
        return id
    }

    public fun setId(id: Long) {
        this.id = id
    }

    public  fun getName(): String? {
        return name
    }

    public  fun setName(name: String?) {
        this.name = name
    }

    public  fun setCount(count: Int) {
        countNum = count
    }

    public  fun getCountNum(): Int {
        return countNum
    }

    public  fun getPath(): String? {
        return path
    }

    /* --------------------------------------------------- */ /* > Parcelable */ /* --------------------------------------------------- */
    public   fun setPath(path: String?) {
        this.path = path
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val image = o as Image
        return image.getPath().equals(getPath(), ignoreCase = true)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
        dest.writeString(path)
    }

    companion object {
        val CREATOR: Parcelable.Creator<Image?> = object : Parcelable.Creator<Image?> {
            override fun createFromParcel(source: Parcel): Image {
                return Image(source)
            }

            override fun newArray(size: Int): Array<Image?> {
                return arrayOfNulls(size)
            }
        }
    }
}
